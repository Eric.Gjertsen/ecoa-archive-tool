import sys
from archive_tool.gui import main

try:
    main()
except Exception as e:
    print(e, file=sys.stderr)
    print("\n\nCheck the log file and report for details.", file=sys.stderr)
    sys.exit(1)
