@echo off

REM Make sure you do this first:
REM virtualenv .venv-build && .venv-build\Scripts\activate && pip install . pyinstaller

.venv-build\Scripts\activate && pyinstaller --onefile --name archive-tool archive-tool.py && pyinstaller --onefile --name archive-tool-gui archive-tool-gui.py
