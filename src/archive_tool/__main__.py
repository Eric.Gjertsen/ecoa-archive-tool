from argparse import ArgumentParser
from getpass import getuser
import logging.config
import logging
import os.path
import os
from queue import Queue
import re
import sys
from time import time
from typing import Callable, Iterable

from . import __version__
from .adapter import config_file
from .adapter.logging import setup_logging, queue_listener
from .model.config import Config, ProcessSitesConfig, DistrConfig, MoveAttachmentsConfig
from .model.precount import Precount
from .model.app_event import (
    Run,
    ProcessSiteCRF,
    ProcessSiteCRFTOC,
    ProcessSiteDCRNotMatched,
    ProcessSiteDCR,
    ProcessSiteDistr,
    ProcessSiteDistrNotMatched,
    ProcessSiteDATA,
    ProcessSiteDOC,
    ProcessSitePDF,
    MoveAttachmentsCopyReport,
    MoveAttachmentsCopy,
    MoveAttachmentsMove,
    MoveAttachmentsNotInReport,
    MoveAttachmentsNotInSource,
)
from .model.exceptions import (
    AppError,
    MatchPatternError,
    SourceDirDoesNotExistError,
)
from .util.datetime import utc_from_posix


def main(argv: list[str] = sys.argv[1:]) -> None:
    tstamp = time()

    cmd = ArgumentParser(description=f"eCOA Post-DG Archive Tool (v{__version__})")
    cmd.add_argument("-c", "--config", default="./archive-tool.ini", help="config file")

    raw = cmd.parse_args(argv)
    config = config_file.parse_file(raw.config)
    run_from_config(config, tstamp)


# ------------------------------------------------------------------------------
# Pre-counting input
# ------------------------------------------------------------------------------


def precount_total(config: Config) -> int:
    return precount(config).total(
        copy_from_source=config.move_attachments.copy_from_source,
        copy_report_from_source=config.move_attachments.copy_report_from_source,
    )


def precount(config: Config) -> Precount:
    return precount_process_sites(
        config.process_sites, config.destination_location
    ) + precount_move_attachments(config.move_attachments, config.destination_location)


def precount_process_sites(
    config: ProcessSitesConfig, destination_location: str
) -> Precount:
    from .adapter.filesys import count_files_in_tree

    n = Precount()
    excls = config.source_site_excludes
    for site_dir in os.listdir(config.source_data_path):
        if site_dir.lower() not in excls and os.path.isdir(
            os.path.join(config.source_data_path, site_dir)
        ):
            crf_site_path = config.source_crf_path(site_dir)
            crf_toc = config.source_crf_toc_path(site_dir)
            data_path = config.source_site_data_path(site_dir)
            docs_path = config.source_docs_path(site_dir)
            pdfs_path = config.source_pdfs_path(destination_location, site_dir)

            n = n + Precount(
                crf=count_files_in_tree(crf_site_path),
                crf_toc=(1 if os.path.exists(crf_toc) else 0),
                data=count_files_in_tree(data_path),
                docs=count_files_in_tree(docs_path),
                pdfs=count_files_in_tree(pdfs_path),
            )

    n = n + Precount(dcrs=len(os.listdir(config.source_dcr_path)))
    return n


def precount_move_attachments(
    config: MoveAttachmentsConfig, destination_location: str
) -> Precount:
    from .adapter.filesys import count_files_in_tree

    src_attachments = config.source_attachments_path
    wrk_attachments = config.destination_work_path(destination_location)
    if config.copy_from_source:
        return Precount(attachments=count_files_in_tree(src_attachments))
    else:
        return Precount(attachments=count_files_in_tree(wrk_attachments))


# ------------------------------------------------------------------------------
# Run
# ------------------------------------------------------------------------------


def run_from_config(
    config: Config, tstamp: float, log_handlers: Iterable[logging.Handler] = []
) -> None:
    dtstamp = utc_from_posix(tstamp)

    log_file = config.report.log_file_path(
        config.destination_location,
        client=config.client,
        study=config.study,
        timestamp=dtstamp,
    )

    queue: Queue[logging.LogRecord] = Queue()

    setup_logging(__name__, log_file, queue, log_handlers)
    logger = logging.getLogger(__name__)

    from .adapter import report_db

    db_file = config.report.db_file_path(
        config.destination_location,
        client=config.client,
        study=config.study,
        timestamp=dtstamp,
    )
    db_dir = os.path.dirname(db_file)
    if not os.path.exists(db_dir):
        os.makedirs(db_dir)
    db = report_db.init(db_file)
    app_event_listener = queue_listener(db, tstamp, queue)
    app_event_listener.start()

    try:
        run_event = Run(
            timestamp=tstamp,
            username=getuser(),
            client=config.client,
            study=config.study,
        )

        with run_event.log(logger, "run"):
            process_sites(
                config.process_sites,
                config.destination_location,
                client=config.client,
                study=config.study,
                overwrite=config.overwrite,
                skip_if_exists=config.skip_if_exists,
                fail_fast=config.fail_fast,
            )

            move_attachments(
                config.move_attachments,
                config.destination_location,
                client=config.client,
                study=config.study,
                overwrite=config.overwrite,
                skip_if_exists=config.skip_if_exists,
                fail_fast=config.fail_fast,
            )

    except Exception as e:
        run_event.log_exception(logger, e)
        raise e

    finally:
        app_event_listener.stop()


# ------------------------------------------------------------------------------
# PROCESS SITES
# ------------------------------------------------------------------------------


def process_sites(
    config: ProcessSitesConfig,
    destination_location: str,
    *,
    client: str,
    study: str,
    overwrite: bool,
    skip_if_exists: bool,
    fail_fast: bool,
) -> None:
    excls = config.source_site_excludes
    site_dirs: set[str] = set()
    for site_dir in os.listdir(config.source_data_path):
        if site_dir.lower() not in excls and os.path.isdir(
            os.path.join(config.source_data_path, site_dir)
        ):
            site_dirs.add(site_dir.lower())
            process_site(
                config,
                destination_location,
                client=client,
                study=study,
                site=site_dir,
                overwrite=overwrite,
                skip_if_exists=skip_if_exists,
                fail_fast=fail_fast,
            )

    try:
        dcr_pattern = config.source_dcr_match_pattern
    except re.error:
        raise MatchPatternError("DCR")  # note: caught as runtime exception

    src_path = config.source_dcr_path
    if not os.path.isdir(src_path):
        raise SourceDirDoesNotExistError(
            src_path, label="DCR"
        )  # caught as runtime exception

    for dcr_file in os.listdir(src_path):
        if os.path.isfile(os.path.join(src_path, dcr_file)):
            process_dcr_file(
                config,
                destination_location,
                client=client,
                study=study,
                dcr_file=dcr_file,
                dcr_pattern=dcr_pattern,
                sites=site_dirs,
                overwrite=overwrite,
                skip_if_exists=skip_if_exists,
                fail_fast=fail_fast,
            )

    if config.custom_distr is not None:
        process_distr(
            source_location=config.source_location,
            destination_location=destination_location,
            config=config.custom_distr,
            client=client,
            study=study,
            sites=site_dirs,
            overwrite=overwrite,
            skip_if_exists=skip_if_exists,
            fail_fast=fail_fast,
        )


def process_site(
    config: ProcessSitesConfig,
    destination_location: str,
    *,
    client: str,
    study: str,
    site: str,
    overwrite: bool,
    skip_if_exists: bool,
    fail_fast: bool,
) -> None:
    logger = logging.getLogger(__name__)

    crf_site_path = config.source_crf_path(site)
    dest_crf_site_path = config.destination_crf_path(destination_location, site)
    try:
        process_site_crf(
            logger=logger,
            client=client,
            study=study,
            site=site,
            src_path=crf_site_path,
            dst_path=dest_crf_site_path,
            overwrite=overwrite,
            skip_if_exists=skip_if_exists,
            fail_fast=fail_fast,
        )
    except AppError as e:
        if fail_fast:
            raise e
        else:
            pass

    crf_toc = config.source_crf_toc_path(site)
    dest_crf_toc = config.destination_crf_toc_path(destination_location, site)
    try:
        process_site_crf_toc(
            logger=logger,
            client=client,
            study=study,
            site=site,
            src_file=crf_toc,
            dst_file=dest_crf_toc,
            overwrite=overwrite,
            skip_if_exists=skip_if_exists,
            fail_fast=fail_fast,
        )
    except AppError as e:
        if fail_fast:
            raise e
        else:
            pass

    data_path = config.source_site_data_path(site)
    dest_data_path = config.destination_site_data_path(destination_location, site)
    try:
        process_site_data(
            logger=logger,
            client=client,
            study=study,
            site=site,
            src_path=data_path,
            dst_path=dest_data_path,
            overwrite=overwrite,
            skip_if_exists=skip_if_exists,
            fail_fast=fail_fast,
        )
    except AppError as e:
        if fail_fast:
            raise e
        else:
            pass

    docs_path = config.source_docs_path(site)
    dest_docs_path = config.destination_docs_path(destination_location, site)
    try:
        process_site_docs(
            logger=logger,
            client=client,
            study=study,
            site=site,
            src_path=docs_path,
            dst_path=dest_docs_path,
            overwrite=overwrite,
            skip_if_exists=skip_if_exists,
            fail_fast=fail_fast,
        )
    except AppError as e:
        if fail_fast:
            raise e
        else:
            pass

    pdfs_path = config.source_pdfs_path(destination_location, site)
    dest_pdfs_path = config.destination_pdfs_path(destination_location, site)
    try:
        process_site_pdfs(
            logger=logger,
            client=client,
            study=study,
            site=site,
            src_path=pdfs_path,
            dst_path=dest_pdfs_path,
            overwrite=overwrite,
            skip_if_exists=skip_if_exists,
            fail_fast=fail_fast,
        )
    except AppError as e:
        if fail_fast:
            raise e
        else:
            pass


def process_site_crf(
    *,
    logger: logging.Logger,
    client: str,
    study: str,
    site: str,
    src_path: str,
    dst_path: str,
    overwrite: bool,
    skip_if_exists: bool,
    fail_fast: bool,
) -> None:
    from .adapter.filesys import file_pairs_in_tree, copy_file

    for src_file, dst_file in file_pairs_in_tree(src_path, dst_path):
        try:
            event = ProcessSiteCRF(
                client=client,
                study=study,
                site=site,
                src_file=src_file,
                dst_file=dst_file,
            )
            with event.log(logger, f"copy {src_file} --> {dst_file}") as log_data:
                copy_file(
                    src_file,
                    dst_file,
                    overwrite=overwrite,
                    skip_if_exists=skip_if_exists,
                    log_data=log_data,
                )
        except AppError as e:
            if fail_fast:
                raise e
            else:
                pass


def process_site_crf_toc(
    *,
    logger: logging.Logger,
    client: str,
    study: str,
    site: str,
    src_file: str,
    dst_file: str,
    overwrite: bool,
    skip_if_exists: bool,
    fail_fast: bool,
) -> None:
    from .adapter.filesys import copy_file

    event = ProcessSiteCRFTOC(
        client=client,
        study=study,
        site=site,
        src_file=src_file,
        dst_file=dst_file,
    )
    with event.log(logger, f"copy {src_file} --> {dst_file}") as log_data:
        copy_file(
            src_file,
            dst_file,
            overwrite=overwrite,
            skip_if_exists=skip_if_exists,
            log_data=log_data,
        )


def process_site_data(
    *,
    logger: logging.Logger,
    client: str,
    study: str,
    site: str,
    src_path: str,
    dst_path: str,
    overwrite: bool,
    skip_if_exists: bool,
    fail_fast: bool,
) -> None:
    from .adapter.filesys import file_pairs_in_tree, copy_file

    for src_file, dst_file in file_pairs_in_tree(src_path, dst_path):
        try:
            event = ProcessSiteDATA(
                client=client,
                study=study,
                site=site,
                src_file=src_file,
                dst_file=dst_file,
            )
            with event.log(logger, f"copy {src_file} --> {dst_file}") as log_data:
                copy_file(
                    src_file,
                    dst_file,
                    overwrite=overwrite,
                    skip_if_exists=skip_if_exists,
                    log_data=log_data,
                )
        except AppError as e:
            if fail_fast:
                raise e
            else:
                pass


def process_site_docs(
    *,
    logger: logging.Logger,
    client: str,
    study: str,
    site: str,
    src_path: str,
    dst_path: str,
    overwrite: bool,
    skip_if_exists: bool,
    fail_fast: bool,
) -> None:
    from .adapter.filesys import file_pairs_in_tree, copy_file

    for src_file, dst_file in file_pairs_in_tree(src_path, dst_path):
        try:
            event = ProcessSiteDOC(
                client=client,
                study=study,
                site=site,
                src_file=src_file,
                dst_file=dst_file,
            )
            with event.log(logger, f"copy {src_file} --> {dst_file}") as log_data:
                copy_file(
                    src_file,
                    dst_file,
                    overwrite=overwrite,
                    skip_if_exists=skip_if_exists,
                    log_data=log_data,
                )
        except AppError as e:
            if fail_fast:
                raise e
            else:
                pass


def process_site_pdfs(
    *,
    logger: logging.Logger,
    client: str,
    study: str,
    site: str,
    src_path: str,
    dst_path: str,
    overwrite: bool,
    skip_if_exists: bool,
    fail_fast: bool,
) -> None:
    from .adapter.filesys import file_pairs_in_tree, copy_file

    for src_file, dst_file in file_pairs_in_tree(src_path, dst_path):
        try:
            event = ProcessSitePDF(
                client=client,
                study=study,
                site=site,
                src_file=src_file,
                dst_file=dst_file,
            )
            with event.log(logger, f"copy {src_file} --> {dst_file}") as log_data:
                copy_file(
                    src_file,
                    dst_file,
                    overwrite=overwrite,
                    skip_if_exists=skip_if_exists,
                    log_data=log_data,
                )
        except AppError as e:
            if fail_fast:
                raise e
            else:
                pass


# TODO: refactor using process_distr below


def process_dcr_file(
    config: ProcessSitesConfig,
    destination_location: str,
    *,
    client: str,
    study: str,
    dcr_file: str,
    dcr_pattern: re.Pattern[str],
    sites: set[str],
    overwrite: bool,
    skip_if_exists: bool,
    fail_fast: bool,
) -> None:
    from .adapter.filesys import copy_file

    logger = logging.getLogger(__name__)

    site = process_dcr_file_match_site(
        logger=logger,
        dcr_pattern=dcr_pattern,
        dcr_prefix=config.dcr_prefix,
        sites=sites,
        client=client,
        study=study,
        src_path=config.source_dcr_path,
        dcr_file=dcr_file,
    )

    if site is not None:
        src_file = os.path.join(config.source_dcr_path, dcr_file)
        dst_file = os.path.join(
            config.destination_dcr_path(destination_location, site), dcr_file
        )
        event = ProcessSiteDCR(
            client=client,
            study=study,
            site=site,
            src_file=src_file,
            dst_file=dst_file,
        )
        try:
            with event.log(logger, f"copying {src_file} --> {dst_file}") as log_data:
                copy_file(
                    src_file,
                    dst_file,
                    overwrite=overwrite,
                    skip_if_exists=skip_if_exists,
                    log_data=log_data,
                )
        except AppError as e:
            if fail_fast:
                raise e
            else:
                pass


def process_dcr_file_match_site(
    *,
    logger: logging.Logger,
    dcr_pattern: re.Pattern[str],
    dcr_prefix: str,
    sites: set[str],
    client: str,
    study: str,
    src_path: str,
    dcr_file: str,
) -> str | None:
    src_file = os.path.join(src_path, dcr_file)
    event = ProcessSiteDCRNotMatched(
        client=client,
        study=study,
        src_file=src_file,
    )
    if not dcr_file.lower().startswith(dcr_prefix.lower()):
        event.log_warning(
            logger,
            f"DCR file does not match the expected prefix '{dcr_prefix}': "
            f"{dcr_file}.",
        )
        return None

    # Note: match against filename removing the prefix
    m = re.match(dcr_pattern, dcr_file[len(dcr_prefix) : len(dcr_file)])
    if m is None:
        event.log_warning(
            logger, f"DCR file does not match expected pattern: {dcr_file}."
        )
        return None

    try:
        site: str = m.group(1)
    except IndexError:
        event.log_warning(
            logger,
            "Misconfigured DCR match pattern. "
            "Expecting a pattern with a single match group identifying the "
            "site in the DCR file name. "
            "The syntax is python-flavored regular expression. "
            "Please check your config and try again.",
        )
        return None

    if site.lower() not in sites:
        event.log_warning(
            logger,
            f"Site '{site}' identified by DCR file is unknown: {dcr_file}.",
        )
        return None

    return site


# ------------------------------------------------------------------------------
# GENERIC DISTRIBUTION
# ------------------------------------------------------------------------------


def process_distr(
    *,
    source_location: str,
    destination_location: str,
    config: DistrConfig,
    client: str,
    study: str,
    sites: set[str],
    overwrite: bool,
    skip_if_exists: bool,
    fail_fast: bool,
) -> None:
    # Note: raised errors are handled as AppError runtime exceptions

    try:
        _ = config.distr_file_pattern
    except re.error:
        raise MatchPatternError(config.label)

    if config.is_zipped:
        try:
            _ = config.distr_zip_pattern
        except re.error:
            raise MatchPatternError(f"{config.label} (zip)")

    src_path = config.source_path(source_location)
    if not os.path.isdir(src_path):
        raise SourceDirDoesNotExistError(src_path, label=config.label)

    src_path = config.source_path(source_location)
    for src_file in os.listdir(src_path):
        if os.path.isfile(os.path.join(src_path, src_file)):
            process_distr_file(
                source_file=src_file,
                source_path=src_path,
                destination_location=destination_location,
                config=config,
                client=client,
                study=study,
                sites=sites,
                overwrite=overwrite,
                skip_if_exists=skip_if_exists,
                fail_fast=fail_fast,
            )


def process_distr_file(
    *,
    source_file: str,
    source_path: str,
    destination_location: str,
    config: DistrConfig,
    client: str,
    study: str,
    sites: set[str],
    overwrite: bool,
    skip_if_exists: bool,
    fail_fast: bool,
) -> None:
    from .adapter.filesys import copy_file

    logger = logging.getLogger(__name__)

    site = process_distr_file_match_site(
        logger=logger,
        distr_label=config.label,
        pattern=config.distr_file_pattern,
        prefix=config.prefix,
        sites=sites,
        client=client,
        study=study,
        source_path=source_path,
        source_file=source_file,
    )

    if site is not None:
        src_file = os.path.join(source_path, source_file)
        dst_file = os.path.join(
            config.destination_path(destination_location, site), source_file
        )
        event = ProcessSiteDistr(
            distr_label=config.label,
            client=client,
            study=study,
            site=site,
            src_file=src_file,
            dst_file=dst_file,
        )
        try:
            with event.log(logger, f"copying {src_file} --> {dst_file}") as log_data:
                copy_file(
                    src_file,
                    dst_file,
                    overwrite=overwrite,
                    skip_if_exists=skip_if_exists,
                    log_data=log_data,
                )
        except AppError as e:
            if fail_fast:
                raise e
            else:
                pass


def process_distr_file_match_site(
    *,
    logger: logging.Logger,
    distr_label: str,
    pattern: re.Pattern[str],
    prefix: str,
    sites: set[str],
    client: str,
    study: str,
    source_path: str,
    source_file: str,
) -> str | None:
    src_file = os.path.join(source_path, source_file)
    event = ProcessSiteDistrNotMatched(
        distr_label=distr_label,
        client=client,
        study=study,
        src_file=src_file,
    )
    if not source_file.lower().startswith(prefix.lower()):
        event.log_warning(
            logger,
            f"{distr_label} file does not match the expected prefix '{prefix}': "
            f"{source_file}.",
        )
        return None

    # Note: match against filename removing the prefix
    m = re.match(pattern, source_file[len(prefix) : len(source_file)])
    if m is None:
        event.log_warning(
            logger,
            f"{distr_label} file does not match expected pattern: {source_file}.",
        )
        return None

    try:
        site: str = m.group(1)
    except IndexError:
        event.log_warning(
            logger,
            f"Misconfigured {distr_label} file match pattern. "
            "Expecting a pattern with a single match group identifying the "
            f"site in the {distr_label} file name. "
            "The syntax is python-flavored regular expression. "
            "Please check your config and try again.",
        )
        return None

    if site.lower() not in sites:
        event.log_warning(
            logger,
            f"Site '{site}' identified by {distr_label} file is unknown: {source_file}.",
        )
        return None

    return site


# ------------------------------------------------------------------------------
# MOVE ATTACHMENTS
# ------------------------------------------------------------------------------


def move_attachments(
    config: MoveAttachmentsConfig,
    destination_location: str,
    *,
    client: str,
    study: str,
    overwrite: bool,
    skip_if_exists: bool,
    fail_fast: bool,
) -> None:
    from .adapter import attachments_db

    logger = logging.getLogger(__name__)

    src_report = config.source_attachments_report_path
    dst_report = config.destination_attachments_report_path(destination_location)
    src_attachments = config.source_attachments_path
    wrk_attachments = config.destination_work_path(destination_location)

    if config.copy_report_from_source:
        try:
            move_attachments_copy_report(
                logger=logger,
                client=client,
                study=study,
                src_file=src_report,
                dst_file=dst_report,
                overwrite=overwrite,
                skip_if_exists=skip_if_exists,
                fail_fast=fail_fast,
            )
        except AppError as e:
            if fail_fast:
                raise e
            else:
                pass

    if config.copy_from_source:
        try:
            move_attachments_copy(
                logger=logger,
                client=client,
                study=study,
                src_dir=src_attachments,
                dst_dir=wrk_attachments,
                overwrite=overwrite,
                skip_if_exists=skip_if_exists,
                fail_fast=fail_fast,
            )
        except AppError as e:
            if fail_fast:
                raise e
            else:
                pass

    try:
        db = attachments_db.init(":memory:")
        attachments_db.load_attachments_report(
            db, dst_report, delimiter="|", force_ext=".pdf"
        )
        attachments_db.load_attachments_in(db, wrk_attachments)
    except AppError as e:
        if fail_fast:
            raise e
        else:
            pass

    for spec, attachment in attachments_db.select_attachments_matching_specs(db):
        site = spec.site
        dst_attachment = config.destination_attachments_path(destination_location, site)
        try:
            move_attachments_move(
                logger=logger,
                client=client,
                study=study,
                site=site,
                src_file=attachment,
                dst_file=dst_attachment,
                overwrite=overwrite,
                skip_if_exists=skip_if_exists,
                fail_fast=fail_fast,
            )
        except AppError as e:
            if fail_fast:
                raise e
            else:
                pass

    for fname in attachments_db.select_attachments_not_matching_specs(db):
        event_no_spec = MoveAttachmentsNotInReport(
            client=client,
            study=study,
            src_file=fname,
        )
        event_no_spec.log_warning(
            logger, f"attachment not in report: {os.path.basename(fname)}"
        )

    for spec in attachments_db.select_specs_not_matching_any_attachments(db):
        event_no_attachment = MoveAttachmentsNotInSource(
            client=client,
            study=study,
            dcf_id=spec.dcf_id,
            repository_id=spec.repository_id,
            file_name=spec.file_name,
            site=spec.site,
        )
        event_no_attachment.log_warning(
            logger, f"no attachments match report spec: {spec}"
        )


def move_attachments_copy_report(
    *,
    logger: logging.Logger,
    client: str,
    study: str,
    src_file: str,
    dst_file: str,
    overwrite: bool,
    skip_if_exists: bool,
    fail_fast: bool,
) -> None:
    from .adapter.filesys import copy_file

    event = MoveAttachmentsCopyReport(
        client=client,
        study=study,
        src_file=src_file,
        dst_file=dst_file,
    )
    with event.log(logger, f"copy {src_file} --> {dst_file}") as log_data:
        copy_file(
            src_file,
            dst_file,
            overwrite=overwrite,
            skip_if_exists=skip_if_exists,
            log_data=log_data,
        )


def move_attachments_copy(
    *,
    logger: logging.Logger,
    client: str,
    study: str,
    src_dir: str,
    dst_dir: str,
    overwrite: bool,
    skip_if_exists: bool,
    fail_fast: bool,
) -> None:
    from .adapter.filesys import copy_tree

    event = MoveAttachmentsCopy(
        client=client,
        study=study,
        src_dir=src_dir,
        dst_dir=dst_dir,
    )
    with event.log(logger, f"copy {src_dir} --> {dst_dir}") as log_data:
        copy_tree(
            src_dir,
            dst_dir,
            overwrite=overwrite,
            skip_if_exists=skip_if_exists,
            log_data=log_data,
        )


def move_attachments_move(
    *,
    logger: logging.Logger,
    client: str,
    study: str,
    site: str,
    src_file: str,
    dst_file: str,
    overwrite: bool,
    skip_if_exists: bool,
    fail_fast: bool,
) -> None:
    from .adapter.filesys import move_file_to_dir

    event = MoveAttachmentsMove(
        client=client,
        study=study,
        site=site,
        src_file=src_file,
        dst_file=dst_file,
    )
    with event.log(logger, f"move {src_file} --> {dst_file}") as log_data:
        move_file_to_dir(
            src_file,
            dst_file,
            overwrite=overwrite,
            skip_if_exists=skip_if_exists,
            log_data=log_data,
        )


# ------------------------------------------------------------------------------
# HELPERS
# ------------------------------------------------------------------------------


def has_dcr_prefix(prefix: str) -> Callable[[str, str], bool]:
    def _has_dcr_prefix(root: str, fname: str) -> bool:
        return fname.lower().startswith(prefix.lower())

    return _has_dcr_prefix


if __name__ == "__main__":
    main()
