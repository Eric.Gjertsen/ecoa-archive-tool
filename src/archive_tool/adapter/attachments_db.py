import csv
from dataclasses import dataclass
import os.path
import sqlite3
from typing import Any, Tuple

from ..util.validate import str_field

FIELD_DCF_ID = "dcf_id"
FIELD_REPOSITORY_ID = "repository_id"
FIELD_FILE_NAME = "file_name"
FIELD_SITE = "SiteCode"


@dataclass(frozen=True)
class Spec:
    dcf_id: str
    repository_id: str
    file_name: str
    ext_name: str
    site: str

    @property
    def base_name(self) -> str:
        return os.path.splitext(self.file_name)[0]


def init(database: str, **kwargs: Any) -> sqlite3.Connection:
    db: sqlite3.Connection = sqlite3.connect(database, **kwargs)
    create_spec_table(db)
    create_attachment_table(db)
    return db


def load_attachments_report(
    db: sqlite3.Connection, fname: str, *, delimiter: str, force_ext: str | None
) -> None:
    with open(fname, "r", newline="") as f:
        reader = csv.DictReader(f, delimiter=delimiter)
        for row in reader:
            insert_spec(db, row, force_ext=force_ext)


def load_attachments_in(db: sqlite3.Connection, path: str) -> None:
    for root, _, fnames in os.walk(path):
        for fname in fnames:
            insert_attachment_from_filename(db, os.path.join(root, fname))


def select_attachments_matching_specs(db: sqlite3.Connection) -> list[Tuple[Spec, str]]:
    c = db.execute(sql_select_attachments_matching_specs())
    c.row_factory = sqlite3.Row  # type: ignore[assignment]
    return [(parse_spec(row), parse_attachment(row)) for row in c.fetchall()]


def select_attachments_not_matching_specs(db: sqlite3.Connection) -> list[str]:
    c = db.execute(sql_select_attachments_not_matching_specs())
    c.row_factory = sqlite3.Row  # type: ignore[assignment]
    return [parse_attachment(row) for row in c.fetchall()]


def select_specs_not_matching_any_attachments(db: sqlite3.Connection) -> list[Spec]:
    c = db.execute(sql_select_specs_not_matching_any_attachments())
    c.row_factory = sqlite3.Row  # type: ignore[assignment]
    return [parse_spec(row) for row in c.fetchall()]


def create_spec_table(db: sqlite3.Connection) -> None:
    db.execute(
        """
CREATE TABLE IF NOT EXISTS `attachment_spec`
    ( `repository_id` TEXT NOT NULL
    , `dcf_id` TEXT NOT NULL
    , `file_name` TEXT NOT NULL
    , `base_name` TEXT NOT NULL
    , `ext_name` TEXT NOT NULL
    , `site` TEXT NOT NULL
    )
    """
    )

    db.execute(
        """
CREATE INDEX IF NOT EXISTS `attachment_spec_repository_id`
    ON attachment_spec(repository_id)
    """
    )

    db.execute(
        """
CREATE INDEX IF NOT EXISTS `attachment_spec_file_name`
    ON attachment_spec(file_name)
    """
    )


def create_attachment_table(db: sqlite3.Connection) -> None:
    db.execute(
        """
CREATE TABLE IF NOT EXISTS `attachment`
    ( `path_name` TEXT NOT NULL
    , `file_name` TEXT NOT NULL
    , `base_name` TEXT NOT NULL
    , `ext_name` TEXT NOT NULL
    , `last_segment` TEXT
    )
    """
    )

    db.execute(
        """
CREATE INDEX IF NOT EXISTS `attachment_file_name`
    ON attachment(file_name)
    """
    )

    db.execute(
        """
CREATE INDEX IF NOT EXISTS `attachment_base_name`
    ON attachment(base_name)
    """
    )

    db.execute(
        """
CREATE INDEX IF NOT EXISTS `attachment_last_segment`
    ON attachment(last_segment)
    """
    )


def insert_spec(
    db: sqlite3.Connection, row: dict[str, str], *, force_ext: str | None
) -> None:
    dcf_id = row[FIELD_DCF_ID].lower()
    repository_id = row[FIELD_REPOSITORY_ID].lower()
    file_name = row[FIELD_FILE_NAME].lower()
    site = row[FIELD_SITE]
    base_name, ext_name = os.path.splitext(file_name)
    if force_ext is not None:
        ext_name = force_ext.lower()

    db.execute(
        """
INSERT INTO `attachment_spec`
    ( `dcf_id`
    , `repository_id`
    , `file_name`
    , `base_name`
    , `ext_name`
    , `site`
    )
VALUES
    ( ?, ?, ?, ?, ?, ? )
    """,
        (dcf_id, repository_id, file_name, base_name, ext_name, site),
    )


def insert_attachment_from_filename(db: sqlite3.Connection, fname: str) -> None:
    path_name = fname.lower()
    file_name = os.path.basename(path_name)
    base_name, ext_name = os.path.splitext(file_name)
    last_segment = base_name.split("_")[-1]

    db.execute(
        """
INSERT INTO `attachment`
    ( `path_name`
    , `file_name`
    , `base_name`
    , `ext_name`
    , `last_segment`
    )
VALUES
    ( ?, ?, ?, ?, ? )
    """,
        (path_name, file_name, base_name, ext_name, last_segment),
    )


def sql_select_attachments_matching_specs() -> str:
    return """
SELECT a.`dcf_id` AS `spec_dcf_id`
    , a.`repository_id` AS `spec_repository_id`
    , a.`file_name` AS `spec_file_name`
    , a.`base_name` AS `spec_base_name`
    , a.`ext_name` AS `spec_ext_name`
    , a.`site` AS `spec_site`
    , b.`path_name` AS `attachment_path_name`
    , b.`file_name` AS `attachment_file_name`
    , b.`base_name` AS `attachment_base_name`
    , b.`ext_name` AS `attachment_ext_name`
    , b.`last_segment` AS `attachment_last_segment`
FROM `attachment_spec` AS a
INNER JOIN `attachment` AS b ON
    ( a.`repository_id` = b.`last_segment` OR
      a.`file_name` = b.`file_name` OR
      a.`file_name` = b.`base_name` || '_' || a.`dcf_id` || b.`ext_name` OR
      b.`file_name` = a.`base_name` || '_' || a.`repository_id` || a.`ext_name`
    )
    """


def sql_select_attachments_not_matching_specs() -> str:
    return """
SELECT b.`path_name` AS `attachment_path_name`
    , b.`file_name` AS `attachment_file_name`
    , b.`base_name` AS `attachment_base_name`
    , b.`ext_name` AS `attachment_ext_name`
    , b.`last_segment` AS `attachment_last_segment`
FROM `attachment_spec` AS a
RIGHT JOIN `attachment` AS b ON
    ( a.`repository_id` = b.`last_segment` OR
      a.`file_name` = b.`file_name` OR
      a.`file_name` = b.`base_name` || '_' || a.`dcf_id` || b.`ext_name` OR
      b.`file_name` = a.`base_name` || '_' || a.`repository_id` || a.`ext_name`
    )
WHERE
    a.`dcf_id` IS NULL AND
    a.`repository_id` IS NULL AND
    a.`file_name` IS NULL AND
    a.`base_name` IS NULL AND
    a.`ext_name` IS NULL AND
    a.`site` IS NULL
    """


def sql_select_specs_not_matching_any_attachments() -> str:
    return """
SELECT a.`dcf_id` AS `spec_dcf_id`
    , a.`repository_id` AS `spec_repository_id`
    , a.`file_name` AS `spec_file_name`
    , a.`base_name` AS `spec_base_name`
    , a.`ext_name` AS `spec_ext_name`
    , a.`site` AS `spec_site`
FROM `attachment_spec` AS a
LEFT JOIN `attachment` AS b ON
    ( a.`repository_id` = b.`last_segment` OR
      a.`file_name` = b.`file_name` OR
      a.`file_name` = b.`base_name` || '_' || a.`dcf_id` || b.`ext_name` OR
      b.`file_name` = a.`base_name` || '_' || a.`repository_id` || a.`ext_name`
    )
WHERE
    b.`path_name` IS NULL AND
    b.`file_name` IS NULL AND
    b.`base_name` IS NULL AND
    b.`ext_name` IS NULL AND
    b.`last_segment` IS NULL
    """


def parse_spec(row: sqlite3.Row) -> Spec:
    rdata: dict[str, Any] = dict(row)
    return Spec(
        dcf_id=str_field("spec_dcf_id", rdata),
        repository_id=str_field("spec_repository_id", rdata),
        file_name=str_field("spec_file_name", rdata),
        ext_name=str_field("spec_ext_name", rdata),
        site=str_field("spec_site", rdata),
    )


def parse_attachment(row: sqlite3.Row) -> str:
    rdata: dict[str, Any] = dict(row)
    return str_field("attachment_path_name", rdata)
