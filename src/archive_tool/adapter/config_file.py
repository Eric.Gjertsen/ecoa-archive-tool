import tomllib
from typing import Any

import tomli_w

from ..model.config import (
    Config,
    ReportConventions,
    ProcessSitesConfig,
    ProcessSitesSourceDirConventions,
    ProcessSitesDestDirConventions,
    DistrConfig,
    MoveAttachmentsConfig,
    MoveAttachmentsSourceDirConventions,
    MoveAttachmentsDestDirConventions,
    DefaultConfig,
    DefaultProcessSitesConfig,
    DefaultMoveAttachmentsConfig,
)
from ..util.validate import (
    required_field,
    bool_field,
    str_field,
    list_field,
    dict_field,
)

APP_NAME = "archive_tool"


def parse_file(
    fname: str,
    *,
    app_name: str = APP_NAME,
) -> Config:
    with open(fname, "br") as f:
        return parse(tomllib.load(f), app_name=app_name)


def parse_defaults_file(fname: str, *, app_name: str = APP_NAME) -> DefaultConfig:
    with open(fname, "br") as f:
        return parse_defaults(tomllib.load(f), app_name=app_name)


def parse(
    data: dict[str, Any],
    *,
    app_name: str = APP_NAME,
) -> Config:
    return parse_config(required_field(app_name, data), [app_name])


def parse_defaults(data: dict[str, Any], *, app_name: str = APP_NAME) -> DefaultConfig:
    return parse_default_config(required_field(app_name, data), [app_name])


def parse_config(data: dict[str, Any], path: list[str]) -> Config:
    return Config(
        client=str_field("client", data),
        study=str_field("study", data),
        overwrite=bool_field("overwrite", data),
        skip_if_exists=bool_field("skip_if_exists", data),
        fail_fast=bool_field("fail_fast", data),
        destination_location=str_field("destination_location", data),
        report=parse_report_conventions(
            required_field("report", data),
            path + ["report"],
        ),
        process_sites=parse_process_sites(
            required_field("process_sites", data),
            path + ["process_sites"],
        ),
        move_attachments=parse_move_attachments(
            required_field("move_attachments", data),
            path + ["move_attachments"],
        ),
    )


def parse_process_sites(
    data: dict[str, Any],
    path: list[str],
) -> ProcessSitesConfig:
    has_custom_distr = data.get("custom_distr", None) is not None
    return ProcessSitesConfig(
        dcr_prefix=str_field("dcr_prefix", data),
        source_location=str_field("source_location", data),
        source_conventions=parse_process_sites_source_dir_conventions(
            required_field("source_conventions", data), path + ["source_conventions"]
        ),
        destination_conventions=parse_process_sites_dest_dir_conventions(
            required_field("destination_conventions", data),
            path + ["destination_conventions"],
        ),
        custom_distr=(
            None
            if not has_custom_distr
            else parse_distr(dict_field("custom_distr", data), path + ["custom_distr"])
        ),
    )


def parse_move_attachments(
    data: dict[str, Any],
    path: list[str],
) -> MoveAttachmentsConfig:
    return MoveAttachmentsConfig(
        copy_from_source=bool_field("copy_from_source", data),
        copy_report_from_source=bool_field("copy_report_from_source", data),
        attachments_report=str_field("attachments_report", data),
        source_location=str_field("source_location", data),
        report_source_location=str_field("report_source_location", data),
        source_conventions=parse_move_attachments_source_dir_conventions(
            required_field("source_conventions", data), path + ["source_conventions"]
        ),
        destination_conventions=parse_move_attachments_dest_dir_conventions(
            required_field("destination_conventions", data),
            path + ["destination_conventions"],
        ),
    )


def parse_report_conventions(
    data: dict[str, Any], path: list[str]
) -> ReportConventions:
    return ReportConventions(
        log_pattern=str_field("log_pattern", data),
        db_pattern=str_field("db_pattern", data),
        report_pattern=str_field("report_pattern", data),
    )


def parse_process_sites_source_dir_conventions(
    data: dict[str, Any], path: list[str]
) -> ProcessSitesSourceDirConventions:
    return ProcessSitesSourceDirConventions(
        data_dir=str_field("data_dir", data),
        dcr_dir=str_field("dcr_dir", data),
        site_excludes=list_field(str, "site_excludes", data),
        crf_dir_pattern=str_field("crf_dir_pattern", data),
        crf_toc_pattern=str_field("crf_toc_pattern", data),
        dcr_match_pattern=str_field("dcr_match_pattern", data),
        data_dir_pattern=str_field("data_dir_pattern", data),
        docs_dir_pattern=str_field("docs_dir_pattern", data),
        pdfs_dir_pattern=str_field("pdfs_dir_pattern", data),
    )


def parse_process_sites_dest_dir_conventions(
    data: dict[str, Any], path: list[str]
) -> ProcessSitesDestDirConventions:
    return ProcessSitesDestDirConventions(
        crf_dir_pattern=str_field("crf_dir_pattern", data),
        crf_toc_pattern=str_field("crf_toc_pattern", data),
        dcr_dir_pattern=str_field("dcr_dir_pattern", data),
        data_dir_pattern=str_field("data_dir_pattern", data),
        docs_dir_pattern=str_field("docs_dir_pattern", data),
        pdfs_dir_pattern=str_field("pdfs_dir_pattern", data),
    )


def parse_distr(data: dict[str, Any], path: list[str]) -> DistrConfig:
    return DistrConfig(
        label=str_field("label", data),
        source_location=str_field("source_location", data),
        prefix=str_field("prefix", data),
        pattern=str_field("pattern", data),
        destination_pattern=str_field("destination_pattern", data),
        is_zipped=bool_field("is_zipped", data),
        zip_prefix=str_field("zip_prefix", data),
        zip_pattern=str_field("zip_pattern", data),
    )


def parse_move_attachments_source_dir_conventions(
    data: dict[str, Any], path: list[str]
) -> MoveAttachmentsSourceDirConventions:
    return MoveAttachmentsSourceDirConventions(
        attachments_dir=str_field("attachments_dir", data),
    )


def parse_move_attachments_dest_dir_conventions(
    data: dict[str, Any], path: list[str]
) -> MoveAttachmentsDestDirConventions:
    return MoveAttachmentsDestDirConventions(
        work_dir=str_field("work_dir", data),
        dir_pattern=str_field("dir_pattern", data),
    )


def parse_default_config(data: dict[str, Any], path: list[str]) -> DefaultConfig:
    return DefaultConfig(
        overwrite=bool_field("overwrite", data),
        skip_if_exists=bool_field("skip_if_exists", data),
        fail_fast=bool_field("fail_fast", data),
        report=parse_report_conventions(
            required_field("report", data),
            path + ["report"],
        ),
        process_sites=parse_default_process_sites(
            required_field("process_sites", data),
            path + ["process_sites"],
        ),
        move_attachments=parse_default_move_attachments(
            required_field("move_attachments", data),
            path + ["move_attachments"],
        ),
    )


def parse_default_process_sites(
    data: dict[str, Any],
    path: list[str],
) -> DefaultProcessSitesConfig:
    return DefaultProcessSitesConfig(
        source_conventions=parse_process_sites_source_dir_conventions(
            required_field("source_conventions", data), path + ["source_conventions"]
        ),
        destination_conventions=parse_process_sites_dest_dir_conventions(
            required_field("destination_conventions", data),
            path + ["destination_conventions"],
        ),
    )


def parse_default_move_attachments(
    data: dict[str, Any],
    path: list[str],
) -> DefaultMoveAttachmentsConfig:
    return DefaultMoveAttachmentsConfig(
        copy_from_source=bool_field("copy_from_source", data),
        copy_report_from_source=bool_field("copy_report_from_source", data),
        source_conventions=parse_move_attachments_source_dir_conventions(
            required_field("source_conventions", data), path + ["source_conventions"]
        ),
        destination_conventions=parse_move_attachments_dest_dir_conventions(
            required_field("destination_conventions", data),
            path + ["destination_conventions"],
        ),
    )


# ------------------------------------------------------------------------------
# Writing
# ------------------------------------------------------------------------------


def write_file(fname: str, config: Config, *, app_name: str = APP_NAME) -> None:
    with open(fname, "wb") as f:
        data = {app_name: config.to_dict()}
        tomli_w.dump(data, f)
