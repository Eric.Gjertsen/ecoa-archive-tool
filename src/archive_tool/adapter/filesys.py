import logging
import os.path
import os
import shutil
from typing import Callable, Any, Generator, Tuple

from ..model.exceptions import (
    SourceDirDoesNotExistError,
    SourceFileDoesNotExistError,
    DestinationFileExistsError,
    SourceFileExistsInDestinationError,
)

logger = logging.getLogger(__name__)


def always_true(root: str, fname: str) -> bool:
    return True


def count_files_in_tree(
    src: str,
    *,
    include: Callable[[str, str], bool] = always_true,
) -> int:
    try:
        return len(list(files_in_tree(src, include=include)))
    except SourceDirDoesNotExistError:
        return 0


def files_in_tree(
    src: str,
    *,
    include: Callable[[str, str], bool] = always_true,
) -> Generator[str, None, None]:
    if not os.path.exists(src):
        raise SourceDirDoesNotExistError(src)
    for root, _, fs in os.walk(src):
        for f in fs:
            if include(root, f):
                yield os.path.join(root, f)


def file_pairs_in_tree(
    src: str,
    dest: str,
    *,
    include: Callable[[str, str], bool] = always_true,
) -> Generator[Tuple[str, str], None, None]:
    if not os.path.exists(src):
        raise SourceDirDoesNotExistError(src)
    for root, _, fs in os.walk(src):
        for f in fs:
            relroot = os.path.relpath(root, src)
            if include(root, f):
                yield (
                    os.path.join(root, f),
                    os.path.join(dest, os.path.join(relroot, f)),
                )


def copy_file(
    src: str,
    dest: str,
    *,
    overwrite: bool = False,
    skip_if_exists: bool = True,
    log_data: dict[str, Any] = {},
) -> None:
    if not os.path.exists(src):
        raise SourceFileDoesNotExistError(src)
    dest_dir = os.path.dirname(dest)
    if not os.path.exists(dest_dir):
        logger.debug(f"creating: {dest_dir}", extra=log_data)
        os.makedirs(dest_dir)
        logger.debug(f"created: {dest_dir}", extra=log_data)
    if os.path.exists(dest):
        if skip_if_exists:
            logger.debug(f"skipping (already exists): {src} --> {dest}", extra=log_data)
            return
        else:
            if not overwrite:
                raise DestinationFileExistsError(src, dest)
    logger.debug(f"copying: {src} --> {dest}", extra=log_data)
    shutil.copyfile(src, dest)
    logger.info(f"copied: {src} --> {dest}", extra=log_data)


def copy_tree(
    src: str,
    dest: str,
    *,
    overwrite: bool = False,
    skip_if_exists: bool = True,
    log_data: dict[str, Any] = {},
) -> None:
    for src_file, dest_file in file_pairs_in_tree(src, dest):
        copy_file(
            src_file,
            dest_file,
            overwrite=overwrite,
            skip_if_exists=skip_if_exists,
            log_data=log_data,
        )


def move_file_to_dir(
    src: str,
    dest: str,
    *,
    overwrite: bool = False,
    skip_if_exists: bool = True,
    log_data: dict[str, Any] = {},
) -> None:
    if not os.path.exists(dest):
        logger.debug(f"creating: {dest}", extra=log_data)
        os.makedirs(dest)
        logger.debug(f"created: {dest}", extra=log_data)
    if os.path.exists(os.path.join(dest, os.path.basename(src))):
        if skip_if_exists:
            logger.debug(f"skipping (already exists): {src} --> {dest}", extra=log_data)
            return
        else:
            if not overwrite:
                raise SourceFileExistsInDestinationError(src, dest)
    shutil.move(src, dest)
