import copy
from logging.handlers import QueueHandler, QueueListener
from logging import (
    Logger,
    Formatter,
    Handler,
    LogRecord,
    StreamHandler,
    FileHandler,
    getLogger,
    INFO,
    DEBUG,
    PlaceHolder,
)
from queue import Queue
from sqlite3 import Connection
import sys
from typing import Iterable

from ..adapter import report_db
from ..model.app_event import (
    AppEvent,
    decode as decode_app_event,
)

LOG_FORMATTER = Formatter(
    fmt="%(levelname)s | %(asctime)s | %(stage)s | %(message)s",
    datefmt="%Y-%m-%d %H:%M:%S %z",
    defaults={"stage": "-"},
)


def setup_logging(
    logger_name: str,
    log_file: str,
    queue: Queue[LogRecord],
    handlers: Iterable[Handler],
) -> None:
    logger = getLogger(logger_name)

    clear_logging(logger)
    setup_console_logging(logger)
    setup_file_logging(logger, log_file)
    bind_queue_handler(logger, queue)
    for h in handlers:
        logger.addHandler(h)

    logger.setLevel(DEBUG)
    logger.propagate = True

    # inspect_logging()


def setup_console_logging(logger: Logger, level: int = INFO) -> None:
    h = StreamHandler(sys.stderr)
    h.setLevel(level)
    h.setFormatter(LOG_FORMATTER)
    logger.addHandler(h)


def setup_file_logging(logger: Logger, log_file: str, level: int = DEBUG) -> None:
    h = FileHandler(log_file)
    h.setLevel(level)
    h.setFormatter(LOG_FORMATTER)
    logger.addHandler(h)


def clear_logging(logger: Logger) -> None:
    # Note: don't ask me why you have to check the list length here
    while logger.hasHandlers() and len(logger.handlers) > 0:
        logger.removeHandler(logger.handlers[0])


def queue_listener(
    connection: Connection, tstamp: float, queue: Queue[LogRecord]
) -> QueueListener:
    return QueueListener(queue, AppEventHandler(connection, tstamp))


def bind_queue_handler(
    logger: Logger, queue: Queue[LogRecord], formatter: Formatter | None = None
) -> None:
    h = AppEventQueueHandler(queue)
    if formatter is not None:
        h.setFormatter(formatter)
    logger.addHandler(h)


class AppEventQueueHandler(QueueHandler):
    def prepare(self, record: LogRecord) -> LogRecord:
        msg = self.format(record)
        record = copy.copy(record)
        record.message = msg
        record.msg = msg
        record.args = None
        return record


class AppEventHandler(Handler):
    def __init__(self, conn: Connection, tstamp: float):
        self.conn = conn
        self.tstamp = tstamp
        super().__init__()

    def emit(self, record: LogRecord) -> None:
        try:
            event = decode_app_event_from_record(record)
            report_db.insert_event(self.conn, self.tstamp, record, event)
        except Exception as e:
            try:
                report_db.insert_runtime_error(self.conn, self.tstamp, record, e)
            except Exception:
                print(e, file=sys.stderr)


def decode_app_event_from_record(record: LogRecord) -> AppEvent:
    data = record.__dict__
    if record.exc_info is None:
        return decode_app_event(data, None)
    else:
        return decode_app_event(data, record.exc_info[1])


def inspect_logging() -> None:
    # Gracias a https://stackoverflow.com/a/55400327/268977

    for k, v in Logger.manager.loggerDict.items():
        print("+ [%s] {%s} " % (str.ljust(k, 20), str(v.__class__)[8:-2]))
        if not isinstance(v, PlaceHolder):
            for h in v.handlers:
                print("     +++", str(h.__class__)[8:-2])
