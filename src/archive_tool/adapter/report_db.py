import copy
from logging import LogRecord
import sqlite3
from typing import Any

from ..model.app_event import (
    AppEvent,
    Run,
    ProcessSiteCRF,
    ProcessSiteCRFTOC,
    ProcessSiteDCRNotMatched,
    ProcessSiteDCR,
    ProcessSiteDistrNotMatched,
    ProcessSiteDistr,
    ProcessSiteDATA,
    ProcessSiteDOC,
    ProcessSitePDF,
    MoveAttachmentsCopyReport,
    MoveAttachmentsCopy,
    MoveAttachmentsMove,
    MoveAttachmentsNotInReport,
    MoveAttachmentsNotInSource,
)


def init(database: str, **kwargs: Any) -> sqlite3.Connection:
    kw = copy.copy(kwargs)
    kw["check_same_thread"] = False
    kw["autocommit"] = True
    db: sqlite3.Connection = sqlite3.connect(database, **kw)
    create_run_table(db)
    create_runtime_error_table(db)
    create_process_site_table(db)
    create_process_site_warning_table(db)
    create_move_attachment_copy_table(db)
    create_move_attachment_table(db)
    create_move_attachment_warning_table(db)
    return db


def insert_event(
    db: sqlite3.Connection, tstamp: float, record: LogRecord, event: AppEvent
) -> None:
    if isinstance(event, (Run,)):
        create_or_update_run_event(
            db,
            tstamp,
            complete_timestamp=None if not event.complete else record.created,
            username=event.username,
            client=event.client,
            study=event.study,
            complete=event.complete,
            error=event.error,
        )

    elif isinstance(
        event,
        (
            ProcessSiteCRF,
            ProcessSiteCRFTOC,
            ProcessSiteDCR,
            ProcessSiteDATA,
            ProcessSiteDOC,
            ProcessSitePDF,
        ),
    ):
        create_or_update_process_site_event(
            db,
            tstamp,
            last_timestamp=record.created,
            last_message=record.message,
            stage=event.__class__.__name__,
            client=event.client,
            study=event.study,
            site=event.site,
            src_file=event.src_file,
            dst_file=event.dst_file,
            complete=event.complete,
            error=event.error,
        )

    elif isinstance(event, ProcessSiteDCRNotMatched):
        create_process_site_warning_event(
            db,
            tstamp,
            timestamp=record.created,
            message=record.message,
            stage=event.__class__.__name__,
            client=event.client,
            study=event.study,
            src_file=event.src_file,
        )

    elif isinstance(event, ProcessSiteDistr):
        create_or_update_process_site_event(
            db,
            tstamp,
            last_timestamp=record.created,
            last_message=record.message,
            stage=event.__class__.__name__,
            client=event.client,
            study=event.study,
            site=event.site,
            src_file=event.src_file,
            dst_file=event.dst_file,
            distr_label=event.distr_label,
            complete=event.complete,
            error=event.error,
        )

    elif isinstance(event, ProcessSiteDistrNotMatched):
        create_process_site_warning_event(
            db,
            tstamp,
            timestamp=record.created,
            message=record.message,
            stage=event.__class__.__name__,
            client=event.client,
            study=event.study,
            src_file=event.src_file,
            distr_label=event.distr_label,
        )

    elif isinstance(event, MoveAttachmentsCopyReport):
        create_or_update_move_attachment_copy_event(
            db,
            tstamp,
            last_timestamp=record.created,
            last_message=record.message,
            stage=event.__class__.__name__,
            client=event.client,
            study=event.study,
            src_file=event.src_file,
            dst_file=event.dst_file,
            complete=event.complete,
            error=event.error,
        )

    elif isinstance(event, MoveAttachmentsCopy):
        create_or_update_move_attachment_copy_event(
            db,
            tstamp,
            last_timestamp=record.created,
            last_message=record.message,
            stage=event.__class__.__name__,
            client=event.client,
            study=event.study,
            src_file=event.src_dir,
            dst_file=event.dst_dir,
            complete=event.complete,
            error=event.error,
        )

    elif isinstance(event, MoveAttachmentsMove):
        create_or_update_move_attachment_event(
            db,
            tstamp,
            last_timestamp=record.created,
            last_message=record.message,
            stage=event.__class__.__name__,
            client=event.client,
            study=event.study,
            site=event.site,
            src_file=event.src_file,
            dst_file=event.dst_file,
            complete=event.complete,
            error=event.error,
        )

    elif isinstance(event, MoveAttachmentsNotInReport):
        create_move_attachment_warning_event(
            db,
            tstamp,
            timestamp=record.created,
            message=record.message,
            stage=event.__class__.__name__,
            client=event.client,
            study=event.study,
            src_file=event.src_file,
        )

    elif isinstance(event, MoveAttachmentsNotInSource):
        create_move_attachment_warning_event(
            db,
            tstamp,
            timestamp=record.created,
            message=record.message,
            stage=event.__class__.__name__,
            client=event.client,
            study=event.study,
            dcf_id=event.dcf_id,
            repository_id=event.repository_id,
            file_name=event.file_name,
            site=event.site,
        )


def insert_runtime_error(
    db: sqlite3.Connection, tstamp: float, record: LogRecord, error: BaseException
) -> None:
    create_runtime_error(
        db,
        tstamp,
        timestamp=record.created,
        level=record.levelname,
        message=record.message,
        error=error,
    )


def create_run_table(db: sqlite3.Connection) -> None:
    db.executescript(
        """
CREATE TABLE IF NOT EXISTS `run`
   ( `timestamp` INTEGER NOT NULL
   , `complete_timestamp` INTEGER
   , `username` TEXT NOT NULL
   , `client` TEXT NOT NULL
   , `study` TEXT NOT NULL
   , `complete` TINYINT NOT NULL
   , `error` TEXT
   )
;
CREATE UNIQUE INDEX IF NOT EXISTS `run_timestamp` ON `run`(`timestamp`);
"""
    )


def create_runtime_error_table(db: sqlite3.Connection) -> None:
    db.execute(
        """
CREATE TABLE IF NOT EXISTS `runtime_error`
    ( `run_timestamp` INTEGER NOT NULL
    , `timestamp` INTEGER NOT NULL
    , `level` TEXT NOT NULL
    , `message` TEXT NOT NULL
    )
"""
    )


def create_process_site_table(db: sqlite3.Connection) -> None:
    db.executescript(
        """
CREATE TABLE IF NOT EXISTS `process_site`
    ( `run_timestamp` INTEGER NOT NULL
    , `last_timestamp` INTEGER NOT NULL
    , `last_message` TEXT NOT NULL
    , `stage` TEXT NOT NULL
    , `client` TEXT NOT NULL
    , `study` TEXT NOT NULL
    , `site` TEXT NOT NULL
    , `src_file` TEXT NOT NULL
    , `dst_file` TEXT NOT NULL
    , `distr_label` TEXT
    , `complete` TINYINT NOT NULL
    , `error` TEXT
    )
;
CREATE UNIQUE INDEX IF NOT EXISTS `process_site_unq` ON `process_site`
    ( `run_timestamp`
    , `stage`
    , `client`
    , `study`
    , `site`
    , `src_file`
    , `dst_file`
    , `distr_label`
    )
;
"""
    )


def create_process_site_warning_table(db: sqlite3.Connection) -> None:
    db.execute(
        """
CREATE TABLE IF NOT EXISTS `process_site_warning`
    ( `run_timestamp` INTEGER NOT NULL
    , `timestamp` INTEGER NOT NULL
    , `message` TEXT NOT NULL
    , `stage` TEXT NOT NULL
    , `client` TEXT NOT NULL
    , `study` TEXT NOT NULL
    , `src_file` TEXT NOT NULL
    , `site` TEXT
    , `distr_label` TEXT
    )
;
"""
    )


def create_move_attachment_copy_table(db: sqlite3.Connection) -> None:
    db.executescript(
        """
CREATE TABLE IF NOT EXISTS `move_attachment_copy`
    ( `run_timestamp` INTEGER NOT NULL
    , `last_timestamp` INTEGER NOT NULL
    , `last_message` TEXT NOT NULL
    , `stage` TEXT NOT NULL
    , `client` TEXT NOT NULL
    , `study` TEXT NOT NULL
    , `src_file` TEXT NOT NULL
    , `dst_file` TEXT NOT NULL
    , `complete` TINYINT NOT NULL
    , `error` TEXT
    )
;
CREATE UNIQUE INDEX IF NOT EXISTS `move_attachment_copy_unq` ON `move_attachment_copy`
    ( `run_timestamp`
    , `stage`
    , `client`
    , `study`
    , `src_file`
    , `dst_file`
    )
;
"""
    )


def create_move_attachment_table(db: sqlite3.Connection) -> None:
    db.executescript(
        """
CREATE TABLE IF NOT EXISTS `move_attachment`
    ( `run_timestamp` INTEGER NOT NULL
    , `last_timestamp` INTEGER NOT NULL
    , `last_message` TEXT NOT NULL
    , `stage` TEXT NOT NULL
    , `client` TEXT NOT NULL
    , `study` TEXT NOT NULL
    , `site` TEXT NOT NULL
    , `src_file` TEXT NOT NULL
    , `dst_file` TEXT NOT NULL
    , `complete` TINYINT NOT NULL
    , `error` TEXT
    )
;
CREATE UNIQUE INDEX IF NOT EXISTS `move_attachment_unq` ON `move_attachment`
    ( `run_timestamp`
    , `stage`
    , `client`
    , `study`
    , `site`
    , `src_file`
    , `dst_file`
    )
;
"""
    )


def create_move_attachment_warning_table(db: sqlite3.Connection) -> None:
    db.execute(
        """
CREATE TABLE IF NOT EXISTS `move_attachment_warning`
    ( `run_timestamp` INTEGER NOT NULL
    , `timestamp` INTEGER NOT NULL
    , `message` TEXT NOT NULL
    , `stage` TEXT NOT NULL
    , `client` TEXT NOT NULL
    , `study` TEXT NOT NULL
    , `src_file` TEXT
    , `dcf_id` TEXT
    , `repository_id` TEXT
    , `file_name` TEXT
    , `site` TEXT
    )
;
"""
    )


def create_runtime_error(
    db: sqlite3.Connection,
    tstamp: float,
    timestamp: float,
    level: str,
    message: str,
    error: BaseException,
) -> None:
    db.execute(
        """
INSERT INTO `runtime_error`
    ( `run_timestamp`
    , `timestamp`
    , `level`
    , `message`
    , `error`
    )
VALUES
    ( :run_timestamp
    , :timestamp
    , :level
    , :message
    , :error
    )
""",
        {
            "run_timestamp": int(tstamp),
            "timestamp": int(timestamp),
            "level": level,
            "message": message,
            "error": str(error),
        },
    )


def create_or_update_run_event(
    db: sqlite3.Connection,
    tstamp: float,
    complete_timestamp: float | None,
    username: str,
    client: str,
    study: str,
    complete: bool,
    error: BaseException | None,
) -> None:
    db.execute(
        """
INSERT INTO `run`
    ( `timestamp`
    , `complete_timestamp`
    , `username`
    , `client`
    , `study`
    , `complete`
    , `error`
    )
VALUES
    ( :timestamp
    , :complete_timestamp
    , :username
    , :client
    , :study
    , :complete
    , :error
    )
ON CONFLICT (`timestamp`)
DO UPDATE SET
      `timestamp` = :timestamp
    , `complete_timestamp` = :complete_timestamp
    , `username` = :username
    , `client` = :client
    , `study` = :study
    , `complete` = :complete
    , `error` = :error

""",
        {
            "timestamp": int(tstamp),
            "complete_timestamp": (
                None if complete_timestamp is None else int(complete_timestamp)
            ),
            "username": username,
            "client": client,
            "study": study,
            "complete": complete,
            "error": None if error is None else str(error),
        },
    )


def create_or_update_process_site_event(
    db: sqlite3.Connection,
    tstamp: float,
    last_timestamp: float,
    last_message: str,
    stage: str,
    client: str,
    study: str,
    site: str,
    src_file: str,
    dst_file: str,
    complete: bool,
    error: BaseException | None,
    distr_label: str | None = None,
) -> None:
    db.execute(
        """
INSERT INTO `process_site`
    ( `run_timestamp`
    , `last_timestamp`
    , `last_message`
    , `stage`
    , `client`
    , `study`
    , `site`
    , `src_file`
    , `dst_file`
    , `distr_label`
    , `complete`
    , `error`
    )
VALUES
    ( :run_timestamp
    , :last_timestamp
    , :last_message
    , :stage
    , :client
    , :study
    , :site
    , :src_file
    , :dst_file
    , :distr_label
    , :complete
    , :error
    )
ON CONFLICT
    ( `run_timestamp`
    , `stage`
    , `client`
    , `study`
    , `site`
    , `src_file`
    , `dst_file`
    , `distr_label`
    )
DO UPDATE SET
      last_timestamp = :last_timestamp
    , last_message = :last_message
    , stage = :stage
    , client = :client
    , study = :study
    , site = :site
    , src_file = :src_file
    , dst_file = :dst_file
    , distr_label = :distr_label
    , complete = :complete
    , error = :error
;
""",
        {
            "run_timestamp": int(tstamp),
            "last_timestamp": int(last_timestamp),
            "last_message": last_message,
            "stage": stage,
            "client": client,
            "study": study,
            "site": site,
            "src_file": src_file,
            "dst_file": dst_file,
            "distr_label": distr_label,
            "complete": complete,
            "error": None if error is None else str(error),
        },
    )


def create_or_update_move_attachment_copy_event(
    db: sqlite3.Connection,
    tstamp: float,
    last_timestamp: float,
    last_message: str,
    stage: str,
    client: str,
    study: str,
    src_file: str,
    dst_file: str,
    complete: bool,
    error: BaseException | None,
) -> None:
    db.execute(
        """
INSERT INTO `move_attachment_copy`
    ( `run_timestamp`
    , `last_timestamp`
    , `last_message`
    , `stage`
    , `client`
    , `study`
    , `src_file`
    , `dst_file`
    , `complete`
    , `error`
    )
VALUES
    ( :run_timestamp
    , :last_timestamp
    , :last_message
    , :stage
    , :client
    , :study
    , :src_file
    , :dst_file
    , :complete
    , :error
    )
ON CONFLICT
    ( `run_timestamp`
    , `stage`
    , `client`
    , `study`
    , `src_file`
    , `dst_file`
    )
DO UPDATE SET
      last_timestamp = :last_timestamp
    , last_message = :last_message
    , stage = :stage
    , client = :client
    , study = :study
    , src_file = :src_file
    , dst_file = :dst_file
    , complete = :complete
    , error = :error
;
""",
        {
            "run_timestamp": int(tstamp),
            "last_timestamp": int(last_timestamp),
            "last_message": last_message,
            "stage": stage,
            "client": client,
            "study": study,
            "src_file": src_file,
            "dst_file": dst_file,
            "complete": complete,
            "error": None if error is None else str(error),
        },
    )


def create_or_update_move_attachment_event(
    db: sqlite3.Connection,
    tstamp: float,
    last_timestamp: float,
    last_message: str,
    stage: str,
    client: str,
    study: str,
    site: str,
    src_file: str,
    dst_file: str,
    complete: bool,
    error: BaseException | None,
) -> None:
    db.execute(
        """
INSERT INTO `move_attachment`
    ( `run_timestamp`
    , `last_timestamp`
    , `last_message`
    , `stage`
    , `client`
    , `study`
    , `site`
    , `src_file`
    , `dst_file`
    , `complete`
    , `error`
    )
VALUES
    ( :run_timestamp
    , :last_timestamp
    , :last_message
    , :stage
    , :client
    , :study
    , :site
    , :src_file
    , :dst_file
    , :complete
    , :error
    )
ON CONFLICT
    ( `run_timestamp`
    , `stage`
    , `client`
    , `study`
    , `site`
    , `src_file`
    , `dst_file`
    )
DO UPDATE SET
      last_timestamp = :last_timestamp
    , last_message = :last_message
    , stage = :stage
    , client = :client
    , study = :study
    , site = :site
    , src_file = :src_file
    , dst_file = :dst_file
    , complete = :complete
    , error = :error
;
""",
        {
            "run_timestamp": int(tstamp),
            "last_timestamp": int(last_timestamp),
            "last_message": last_message,
            "stage": stage,
            "client": client,
            "study": study,
            "site": site,
            "src_file": src_file,
            "dst_file": dst_file,
            "complete": complete,
            "error": None if error is None else str(error),
        },
    )


def create_process_site_warning_event(
    db: sqlite3.Connection,
    tstamp: float,
    timestamp: float,
    message: str,
    stage: str,
    client: str,
    study: str,
    src_file: str,
    site: str | None = None,
    distr_label: str | None = None,
) -> None:
    db.execute(
        """
INSERT INTO `process_site_warning`
    (  run_timestamp
    ,  timestamp
    ,  message
    ,  stage
    ,  client
    ,  study
    ,  src_file
    ,  site
    ,  distr_label
    )
VALUES
    (  :run_timestamp
    ,  :timestamp
    ,  :message
    ,  :stage
    ,  :client
    ,  :study
    ,  :src_file
    ,  :site
    ,  :distr_label
    )
""",
        {
            "run_timestamp": int(tstamp),
            "timestamp": int(timestamp),
            "message": message,
            "stage": stage,
            "client": client,
            "study": study,
            "src_file": src_file,
            "site": site,
            "distr_label": distr_label,
        },
    )


def create_move_attachment_warning_event(
    db: sqlite3.Connection,
    tstamp: float,
    timestamp: float,
    message: str,
    stage: str,
    client: str,
    study: str,
    src_file: str | None = None,
    dcf_id: str | None = None,
    repository_id: str | None = None,
    file_name: str | None = None,
    site: str | None = None,
) -> None:
    db.execute(
        """
INSERT INTO `move_attachment_warning`
    (  run_timestamp
    ,  timestamp
    ,  message
    ,  stage
    ,  client
    ,  study
    ,  src_file
    ,  dcf_id
    ,  repository_id
    ,  file_name
    ,  site
    )
VALUES
    (  :run_timestamp
    ,  :timestamp
    ,  :message
    ,  :stage
    ,  :client
    ,  :study
    ,  :src_file
    ,  :dcf_id
    ,  :repository_id
    ,  :file_name
    ,  :site
    )
""",
        {
            "run_timestamp": int(tstamp),
            "timestamp": int(timestamp),
            "message": message,
            "stage": stage,
            "client": client,
            "study": study,
            "src_file": src_file,
            "dcf_id": dcf_id,
            "repository_id": repository_id,
            "file_name": file_name,
            "site": site,
        },
    )
