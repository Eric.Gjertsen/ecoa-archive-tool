from argparse import ArgumentParser
from dataclasses import dataclass
import logging
import os.path
import sys
from time import time
from threading import Thread
import tkinter as tk
from tkinter import ttk
from tkinter import filedialog
from typing import Callable

from .adapter import config_file
from .model.config import (
    Config,
    ProcessSitesConfig,
    MoveAttachmentsConfig,
    DistrConfig,
    DefaultConfig,
    DefaultProcessSitesConfig,
    DefaultMoveAttachmentsConfig,
)

from . import __version__
from .__main__ import run_from_config, precount_total


def main(argv: list[str] = sys.argv[1:]) -> None:
    cmd = ArgumentParser(description=f"eCOA Post-DG Archive Tool GUI (v{__version__})")
    cmd.add_argument(
        "-d", "--defaults", default="./archive-tool.ini", help="config defaults file"
    )
    cmd.add_argument("-c", "--config", help="config file")

    raw = cmd.parse_args(argv)

    config: Config | None = None
    if raw.config is None:
        if raw.defaults is None:
            raise ValueError("No config or default config file specified")
        default_config = config_file.parse_defaults_file(raw.defaults)
        run_gui_from_default_config(default_config)
    else:
        config = config_file.parse_file(raw.config)
        run_gui_from_config(raw.config, config)


def run_gui_from_config(config_fname: str, config: Config) -> None:
    window = tk.Tk()
    entry = ConfigEntry.from_config(config)
    default_config = config.to_default()
    run_gui(window, entry, default_config)


def run_gui_from_default_config(default_config: DefaultConfig) -> None:
    window = tk.Tk()
    entry = ConfigEntry.default(default_config)
    run_gui(window, entry, default_config)


def run_gui(window: tk.Tk, entry: "ConfigEntry", default_config: DefaultConfig) -> None:
    window.title("Post-DG Archive Tool")
    # window.geometry('600x325')
    window.resizable(True, True)

    app = App(window, entry, default_config)
    app.run()


# ------------------------------------------------------------------------------
# Runner thread
# ------------------------------------------------------------------------------


class Runner(Thread):
    def __init__(self, config: Config, log_handler: logging.Handler):
        super().__init__()
        self.config = config
        self.log_handler = log_handler

    def run(self) -> None:
        run_from_config(self.config, time(), log_handlers=[self.log_handler])


# ------------------------------------------------------------------------------
# Entry variable models
# ------------------------------------------------------------------------------


@dataclass
class GeneralEntry:
    client: tk.StringVar
    study: tk.StringVar
    overwrite: tk.BooleanVar
    skip_if_exists: tk.BooleanVar
    fail_fast: tk.BooleanVar
    destination: tk.StringVar

    @classmethod
    def default(cls, defaults: DefaultConfig) -> "GeneralEntry":
        return cls(
            client=tk.StringVar(value=""),
            study=tk.StringVar(value=""),
            overwrite=tk.BooleanVar(value=defaults.overwrite),
            skip_if_exists=tk.BooleanVar(value=defaults.skip_if_exists),
            fail_fast=tk.BooleanVar(value=defaults.fail_fast),
            destination=tk.StringVar(value=""),
        )

    @classmethod
    def from_config(cls, config: Config) -> "GeneralEntry":
        return cls(
            client=tk.StringVar(value=config.client),
            study=tk.StringVar(value=config.study),
            overwrite=tk.BooleanVar(value=config.overwrite),
            skip_if_exists=tk.BooleanVar(value=config.skip_if_exists),
            fail_fast=tk.BooleanVar(value=config.fail_fast),
            destination=tk.StringVar(value=config.destination_location),
        )

    def valid(self) -> bool:
        return (
            nonempty_string_var(self.client)
            and nonempty_string_var(self.study)
            and nonempty_string_var(self.destination)
        )

    def update(self, config: Config) -> None:
        self.client.set(config.client)
        self.study.set(config.study)
        self.overwrite.set(config.overwrite)
        self.skip_if_exists.set(config.skip_if_exists)
        self.fail_fast.set(config.fail_fast)
        self.destination.set(config.destination_location)


@dataclass
class ProcessSitesEntry:
    source_location: tk.StringVar
    dcr_prefix: tk.StringVar

    @classmethod
    def default(cls, defaults: DefaultProcessSitesConfig) -> "ProcessSitesEntry":
        return cls(
            source_location=tk.StringVar(value=""),
            dcr_prefix=tk.StringVar(value=""),
        )

    @classmethod
    def from_config(cls, config: ProcessSitesConfig) -> "ProcessSitesEntry":
        return cls(
            source_location=tk.StringVar(value=config.source_location),
            dcr_prefix=tk.StringVar(value=config.dcr_prefix),
        )

    def valid(self) -> bool:
        return nonempty_string_var(self.source_location) and nonempty_string_var(
            self.dcr_prefix
        )

    def update(self, config: ProcessSitesConfig) -> None:
        self.source_location.set(config.source_location)
        self.dcr_prefix.set(config.dcr_prefix)


@dataclass
class MoveAttachmentsEntry:
    copy_from_source: tk.BooleanVar
    copy_report_from_source: tk.BooleanVar
    attachments_report: tk.StringVar
    source_location: tk.StringVar
    report_source_location: tk.StringVar

    @classmethod
    def default(cls, defaults: DefaultMoveAttachmentsConfig) -> "MoveAttachmentsEntry":
        return cls(
            copy_from_source=tk.BooleanVar(value=defaults.copy_from_source),
            copy_report_from_source=tk.BooleanVar(
                value=defaults.copy_report_from_source
            ),
            attachments_report=tk.StringVar(value=""),
            source_location=tk.StringVar(value=""),
            report_source_location=tk.StringVar(value=""),
        )

    @classmethod
    def from_config(cls, config: MoveAttachmentsConfig) -> "MoveAttachmentsEntry":
        return cls(
            copy_from_source=tk.BooleanVar(value=config.copy_from_source),
            copy_report_from_source=tk.BooleanVar(value=config.copy_report_from_source),
            attachments_report=tk.StringVar(value=config.attachments_report),
            source_location=tk.StringVar(value=config.source_location),
            report_source_location=tk.StringVar(value=config.report_source_location),
        )

    def valid(self) -> bool:
        return (
            nonempty_string_var(self.attachments_report)
            and (
                not self.copy_from_source.get()
                or nonempty_string_var(self.source_location)
            )
            and (
                not self.copy_report_from_source.get()
                or nonempty_string_var(self.report_source_location)
            )
        )

    def update(self, config: MoveAttachmentsConfig) -> None:
        self.copy_from_source.set(config.copy_from_source)
        self.copy_report_from_source.set(config.copy_report_from_source)
        self.attachments_report.set(config.attachments_report)
        self.source_location.set(config.source_location)
        self.report_source_location.set(config.report_source_location)


@dataclass
class CustomDistrEntry:
    distr_label: tk.StringVar
    source_location: tk.StringVar
    destination_pattern: tk.StringVar
    file_prefix: tk.StringVar
    file_pattern: tk.StringVar
    is_zipped: tk.BooleanVar
    zip_prefix: tk.StringVar
    zip_pattern: tk.StringVar

    @classmethod
    def default(self) -> "CustomDistrEntry":
        return CustomDistrEntry(
            distr_label=tk.StringVar(value=""),
            source_location=tk.StringVar(value=""),
            destination_pattern=tk.StringVar(value=""),
            file_prefix=tk.StringVar(value=""),
            file_pattern=tk.StringVar(value=""),
            is_zipped=tk.BooleanVar(value=False),
            zip_prefix=tk.StringVar(value=""),
            zip_pattern=tk.StringVar(value=""),
        )

    @classmethod
    def from_config(cls, config: DistrConfig) -> "CustomDistrEntry":
        return cls(
            distr_label=tk.StringVar(value=config.label),
            source_location=tk.StringVar(value=config.source_location),
            destination_pattern=tk.StringVar(value=config.destination_pattern),
            file_prefix=tk.StringVar(value=config.prefix),
            file_pattern=tk.StringVar(value=config.pattern),
            is_zipped=tk.BooleanVar(value=config.is_zipped),
            zip_prefix=tk.StringVar(value=config.zip_prefix),
            zip_pattern=tk.StringVar(value=config.zip_pattern),
        )

    def valid(self) -> bool:
        return (
            nonempty_string_var(self.distr_label)
            and nonempty_string_var(self.source_location)
            and nonempty_string_var(self.destination_pattern)
            and nonempty_string_var(self.file_pattern)
            and (not self.is_zipped.get() or nonempty_string_var(self.zip_pattern))
        )


@dataclass
class ConfigEntry:
    general: GeneralEntry
    process_sites: ProcessSitesEntry
    move_attachments: MoveAttachmentsEntry
    has_custom_distr: tk.BooleanVar
    custom_distr: CustomDistrEntry
    is_running: bool = False

    @classmethod
    def default(cls, defaults: DefaultConfig) -> "ConfigEntry":
        return cls(
            general=GeneralEntry.default(defaults),
            process_sites=ProcessSitesEntry.default(defaults.process_sites),
            move_attachments=MoveAttachmentsEntry.default(defaults.move_attachments),
            has_custom_distr=tk.BooleanVar(value=False),  # TODO
            custom_distr=CustomDistrEntry.default(),  # TODO
            is_running=False,
        )

    @classmethod
    def from_config(cls, config: Config) -> "ConfigEntry":
        return cls(
            general=GeneralEntry.from_config(config),
            process_sites=ProcessSitesEntry.from_config(config.process_sites),
            move_attachments=MoveAttachmentsEntry.from_config(config.move_attachments),
            has_custom_distr=tk.BooleanVar(
                value=config.process_sites.custom_distr is not None
            ),
            custom_distr=(
                CustomDistrEntry.default()
                if config.process_sites.custom_distr is None
                else CustomDistrEntry.from_config(config.process_sites.custom_distr)
            ),
            is_running=False,
        )

    def valid(self) -> bool:
        return (
            self.general.valid()
            and self.process_sites.valid()
            and self.move_attachments.valid()
            and (not self.has_custom_distr.get() or self.custom_distr.valid())
        )

    def to_config(self, default: DefaultConfig) -> Config | None:
        # TODO: add custom distr to config
        if not self.valid():
            return None
        return Config(
            client=self.general.client.get(),
            study=self.general.study.get(),
            overwrite=self.general.overwrite.get(),
            skip_if_exists=self.general.skip_if_exists.get(),
            fail_fast=self.general.fail_fast.get(),
            destination_location=self.general.destination.get(),
            report=default.report,
            process_sites=ProcessSitesConfig(
                dcr_prefix=self.process_sites.dcr_prefix.get(),
                source_location=self.process_sites.source_location.get(),
                source_conventions=default.process_sites.source_conventions,
                destination_conventions=default.process_sites.destination_conventions,
                custom_distr=(
                    None
                    if not self.has_custom_distr.get()
                    else DistrConfig(
                        label=self.custom_distr.distr_label.get(),
                        source_location=self.custom_distr.source_location.get(),
                        destination_pattern=self.custom_distr.destination_pattern.get(),
                        prefix=self.custom_distr.file_prefix.get(),
                        pattern=self.custom_distr.file_pattern.get(),
                        is_zipped=self.custom_distr.is_zipped.get(),
                        zip_prefix=self.custom_distr.zip_prefix.get(),
                        zip_pattern=self.custom_distr.zip_pattern.get(),
                    )
                ),
            ),
            move_attachments=MoveAttachmentsConfig(
                copy_from_source=self.move_attachments.copy_from_source.get(),
                copy_report_from_source=self.move_attachments.copy_report_from_source.get(),
                attachments_report=self.move_attachments.attachments_report.get(),
                source_location=self.move_attachments.source_location.get(),
                report_source_location=self.move_attachments.report_source_location.get(),
                source_conventions=default.move_attachments.source_conventions,
                destination_conventions=default.move_attachments.destination_conventions,
            ),
        )

    def update(self, config: Config) -> None:
        self.general.update(config)
        self.process_sites.update(config.process_sites)
        self.move_attachments.update(config.move_attachments)


def nonempty_string_var(v: tk.StringVar) -> bool:
    value = v.get()
    return isinstance(value, str) and len(value) > 0


# ------------------------------------------------------------------------------
# Progress bar log handler
# ------------------------------------------------------------------------------

PROGRESS_BAR_LOG_FORMATTER = logging.Formatter(
    fmt="%(stage)s %(site)s: %(message)s",
    defaults={"stage": "", "site": ""},
)


class ProgressBarLogHandler(logging.Handler):
    def __init__(
        self,
        level: int,
        *,
        pbar: ttk.Progressbar,
        label: ttk.Label,
    ):
        super().__init__(level)
        self.had_warning = False
        self.had_error = False
        self.pbar = pbar
        self.label = label
        self.init_styles()

    def init_styles(self) -> None:
        s = ttk.Style()
        s.theme_use("clam")
        s.configure(
            "default.Horizontal.TProgressbar", foreground="green", background="green"
        )
        s.configure(
            "warning.Horizontal.TProgressbar", foreground="yellow", background="yellow"
        )
        s.configure("error.Horizontal.TProgressbar", foreground="red", background="red")

    def emit(self, record: logging.LogRecord) -> None:
        level = record.levelno
        log_msg = self.format(record)
        self.had_warning = self.had_warning or (level == logging.WARNING)
        self.had_error = self.had_error or (level >= logging.ERROR)
        self.update_display(level, log_msg)

    def update_display(self, level: int, msg: str) -> None:
        new_steps = min(1, self.pbar["maximum"] - self.pbar["value"] - 0.001)
        if new_steps > 0:
            self.pbar.step(new_steps)

        if self.had_error:
            self.pbar.configure(style="error.Horizontal.TProgressbar")
        elif self.had_warning:
            self.pbar.configure(style="warning.Horizontal.TProgressbar")
        else:
            self.pbar.configure(style="default.Horizontal.TProgressbar")
        self.label.config(text=ellipsis(50, msg))


def ellipsis(length: int, s: str) -> str:
    return s if len(s) <= length else f"{s[0:(length-3)]}..."


# ------------------------------------------------------------------------------
# Tk App
# ------------------------------------------------------------------------------


class App:
    def __init__(
        self,
        master: tk.Tk | None,
        state: ConfigEntry,
        defaults: DefaultConfig,
        last_config_file: str | None = None,
    ):
        self._root = tk.Frame(master)
        self._root.pack(fill="both", expand=True)
        self.state = state
        self.defaults = defaults
        self.last_config_file = last_config_file
        self.log_viewer: ttk.Frame | None = None

        buttons = self.create_buttons()
        tabs = self.create_tabs()

        self.refresh()

        tabs.pack(fill="x", expand=True)
        buttons.pack(fill="x", expand=True)
        self.reset_log_viewer()

    @property
    def general_entry(self) -> GeneralEntry:
        return self.state.general

    @property
    def process_sites_entry(self) -> ProcessSitesEntry:
        return self.state.process_sites

    @property
    def move_attachments_entry(self) -> MoveAttachmentsEntry:
        return self.state.move_attachments

    @property
    def has_custom_distr_entry(self) -> tk.BooleanVar:
        return self.state.has_custom_distr

    @property
    def custom_distr_entry(self) -> CustomDistrEntry:
        return self.state.custom_distr

    @property
    def is_running(self) -> bool:
        return self.state.is_running

    def run(self) -> None:
        self._root.mainloop()

    def entry_valid(self) -> bool:
        return self.state.valid()

    def refresh_validate(self) -> bool:
        """use for field validate callbacks"""
        enabled = self.entry_valid() and not self.is_running
        for b in [self.run_button, self.save_button]:
            b.configure(state=tk.NORMAL if enabled else tk.DISABLED)

        return True

    def refresh(self) -> None:
        """use for button command callbacks"""
        self.refresh_validate()

    def run_command(self) -> None:
        config = self.state.to_config(self.defaults)
        if config is None:
            return  # should not be reachable since run button only enabled if entry.valid()

        max_steps = precount_total(config)
        pbar, label = self.reset_log_viewer(max_steps)

        h = ProgressBarLogHandler(logging.INFO, pbar=pbar, label=label)
        h.setFormatter(PROGRESS_BAR_LOG_FORMATTER)

        runner = Runner(config, h)
        runner.start()
        self.monitor(runner, lambda: pbar.step(pbar["maximum"] - pbar["value"]))

    def monitor(self, t: Thread, final: Callable[[], None]) -> None:
        if t.is_alive():
            self.after(1000, lambda: self.monitor(t, final))  # type: ignore[attr-defined]
        else:
            t.join()
            final()

        """
        try:
            run_from_config(config, time(), log_handlers=[h])

        except Exception as e:
            messagebox.showerror(
                message=(
                    f"Note: an error occured during processing {config.client} {config.study}. "
                    "Please check log files and report for details.\n\n"
                    f"{e}"
                ),
                title="Run",
            )

        else:
            messagebox.showinfo(
                message=(
                    f"Processing complete for {config.client} {config.study} "
                    "Please check log files and report for any warnings."
                ),
                title="Run",
            )
        """

    def save_command(self) -> None:
        c = self.state.to_config(self.defaults)
        if c is None:
            return
        initdir: str
        initfile: str | None = None
        if self.last_config_file is None:
            initdir = self.state.general.destination.get()
            initfile = None
        else:
            initdir, initfile = os.path.split(self.last_config_file)

        fname = filedialog.asksaveasfilename(
            title="Save to config file",
            initialdir=None if not os.path.exists(initdir) else initdir,
            filetypes=[("ini files", "*.ini"), ("toml files", "*.toml")],
            defaultextension=".ini",
        )
        if fname is None:
            return
        config_file.write_file(fname, c)

    def load_command(self) -> None:
        initdir = self.state.general.destination.get()
        fname = filedialog.askopenfilename(
            title="Load from config file",
            initialdir=None if not os.path.exists(initdir) else initdir,
            filetypes=[("ini files", "*.ini"), ("toml files", "*.toml")],
        )
        if fname is None:
            return
        c = config_file.parse_file(fname)
        self.state.update(c)
        self.refresh()

    def create_buttons(self) -> ttk.Frame:
        root = ttk.Frame(self._root, padding=(10,))
        run_button = ttk.Button(root, text="Run", width=-15, command=self.run_command)
        save_button = ttk.Button(
            root, text="Save to file", width=-15, command=self.save_command
        )
        load_button = ttk.Button(
            root, text="Load from file", width=-15, command=self.load_command
        )

        run_button.pack(side=tk.RIGHT, padx=5)
        save_button.pack(side=tk.RIGHT, padx=5)
        load_button.pack(side=tk.RIGHT, padx=5)

        # references needed for self.refresh
        self.run_button = run_button
        self.save_button = save_button
        self.load_button = load_button

        return root

    def create_tabs(self) -> ttk.Notebook:
        nb = ttk.Notebook(self._root, padding=(10,))
        nb.enable_traversal()

        nb.add(
            self.create_general_tab(nb, self.general_entry),
            text=" General ",
            underline=1,
            padding=(10,),
        )
        nb.add(
            self.create_process_sites_tab(nb, self.process_sites_entry),
            text=" Process Sites ",
            underline=1,
            padding=(10,),
        )
        nb.add(
            self.create_move_attachments_tab(nb, self.move_attachments_entry),
            text=" Move Attachments ",
            underline=1,
            padding=(10,),
        )
        nb.add(
            self.create_custom_distr_tab(
                nb, self.has_custom_distr_entry, self.custom_distr_entry
            ),
            text=" Custom Distribution ",
            underline=1,
            padding=(10,),
        )
        return nb

    def create_general_tab(self, root: ttk.Notebook, entry: GeneralEntry) -> ttk.Frame:
        frame = ttk.Frame(root, padding=(10,))
        lbl_client = ttk.Label(frame, text="Client")
        lbl_study = ttk.Label(frame, text="Study")
        lbl_destination = ttk.Label(frame, text="Destination location")
        entry_client = ttk.Entry(frame, textvariable=entry.client)
        entry_study = ttk.Entry(frame, textvariable=entry.study)
        entry_destination = create_directory_picker(
            frame,
            textvariable=entry.destination,
            title="Select destination directory",
            refresh=self.refresh_validate,
        )
        entry_overwrite = ttk.Checkbutton(
            frame,
            text="Overwrite",
            variable=entry.overwrite,
            onvalue=True,
            offvalue=False,
        )
        entry_skip_if_exists = ttk.Checkbutton(
            frame,
            text="Skip if exists",
            variable=entry.skip_if_exists,
            onvalue=True,
            offvalue=False,
        )
        entry_fail_fast = ttk.Checkbutton(
            frame,
            text="Fail fast",
            variable=entry.fail_fast,
            onvalue=True,
            offvalue=False,
        )

        lbl_client.pack(fill="x", expand=False)
        entry_client.pack(fill="x", expand=False, pady=(0, 10))
        lbl_study.pack(fill="x", expand=False)
        entry_study.pack(fill="x", expand=False, pady=(0, 10))
        lbl_destination.pack(fill="x", expand=False)
        entry_destination.pack(fill="x", expand=False)
        entry_overwrite.pack(fill="x", expand=False)
        entry_skip_if_exists.pack(fill="x", expand=False)
        entry_fail_fast.pack(fill="x", expand=False)

        return frame

    def create_process_sites_tab(
        self, root: ttk.Notebook, entry: ProcessSitesEntry
    ) -> ttk.Frame:
        frame = ttk.Frame(root, padding=(10,))
        lbl_src_location = ttk.Label(frame, text="Source location")
        lbl_dcr_prefix = ttk.Label(frame, text="DCR prefix")
        entry_src_location = create_directory_picker(
            frame,
            textvariable=entry.source_location,
            title="Select source location",
            refresh=self.refresh_validate,
        )
        entry_dcr_prefix = ttk.Entry(frame, textvariable=entry.dcr_prefix)

        # Update global button state on focusout
        entry_dcr_prefix.configure(
            validate="focusout",
            validatecommand=entry_dcr_prefix.register(self.refresh_validate),
        )

        lbl_src_location.pack(fill="x", expand=False)
        entry_src_location.pack(fill="x", expand=False, pady=(0, 10))
        lbl_dcr_prefix.pack(fill="x", expand=False)
        entry_dcr_prefix.pack(fill="x", expand=False, pady=(0, 10))

        return frame

    def create_move_attachments_tab(
        self, root: ttk.Notebook, entry: MoveAttachmentsEntry
    ) -> ttk.Frame:
        frame = ttk.Frame(root, padding=(10,))
        lbl_report_name = ttk.Label(frame, text="Attachments report name")
        entry_report_name = ttk.Entry(frame, textvariable=entry.attachments_report)
        entry_copy_from_source = ttk.Checkbutton(
            frame,
            text="Copy attachments from source location",
            variable=entry.copy_from_source,
            onvalue=True,
            offvalue=False,
        )
        entry_copy_report_from_source = ttk.Checkbutton(
            frame,
            text="Copy attachments report from source location",
            variable=entry.copy_report_from_source,
            onvalue=True,
            offvalue=False,
        )
        entry_src_location = create_directory_picker(
            frame,
            textvariable=entry.source_location,
            title="Select attachments source location",
            refresh=self.refresh_validate,
        )
        entry_report_src_location = create_directory_picker(
            frame,
            textvariable=entry.report_source_location,
            title="Select attachments report source location",
            refresh=self.refresh_validate,
        )

        # Default state
        set_frame_state(entry_src_location)(entry_copy_from_source.state())
        set_frame_state(entry_report_src_location)(
            entry_copy_report_from_source.state()
        )

        # Event handlers
        entry_copy_from_source.configure(
            command=multi_commands(
                [
                    on_toggle(
                        entry_copy_from_source, set_frame_state(entry_src_location)
                    ),
                    self.refresh,
                ]
            )
        )
        entry_copy_report_from_source.configure(
            command=multi_commands(
                [
                    on_toggle(
                        entry_copy_report_from_source,
                        set_frame_state(entry_report_src_location),
                    ),
                    self.refresh,
                ]
            )
        )

        # Update global button state on focusout
        entry_report_name.configure(
            validate="focusout",
            validatecommand=entry_report_name.register(self.refresh_validate),
        )

        lbl_report_name.pack(fill="x", expand=False)
        entry_report_name.pack(fill="x", expand=False, pady=(0, 10))
        entry_copy_from_source.pack(fill="x", expand=False)
        entry_src_location.pack(fill="x", expand=False, pady=(0, 10))
        entry_copy_report_from_source.pack(fill="x", expand=False)
        entry_report_src_location.pack(fill="x", expand=False, pady=(0, 10))

        return frame

    def create_custom_distr_tab(
        self, root: ttk.Notebook, has_entry: tk.BooleanVar, entry: CustomDistrEntry
    ) -> ttk.Frame:
        frame = ttk.Frame(root, padding=(10,))
        entry_has_custom_distr = ttk.Checkbutton(
            frame,
            text="Include custom file distribution",
            variable=has_entry,
            onvalue=True,
            offvalue=False,
        )

        frame_body = self.create_custom_distr_frame_body(frame, entry)

        # Default state
        set_frame_state(frame_body)(entry_has_custom_distr.state())

        # Event handlers
        entry_has_custom_distr.configure(
            command=multi_commands(
                [
                    on_toggle(entry_has_custom_distr, set_frame_state(frame_body)),
                    self.refresh,
                ]
            )
        )

        entry_has_custom_distr.pack(fill="x", expand=False, pady=(0, 10))
        frame_body.pack(fill="x", expand=False)

        return frame

    def create_custom_distr_frame_body(
        self, root: ttk.Frame, entry: CustomDistrEntry
    ) -> ttk.Frame:
        frame = ttk.Frame(root)

        """
        lbl_load_preset = ttk.Label(frame, text="Load preset distribution")
        entry_load_preset = ttk.Combobox(
            frame,
            state="readonly",  # dropdown behavior only
            values=["SleepIZ"],
        )
        """
        lbl_distr_label = ttk.Label(frame, text="Label")
        entry_distr_label = ttk.Entry(frame, textvariable=entry.distr_label)
        lbl_src_location = ttk.Label(frame, text="Source location")
        entry_src_location = create_directory_picker(
            frame,
            textvariable=entry.source_location,
            title="Select source directory",
            refresh=self.refresh_validate,
        )
        lbl_file_prefix = ttk.Label(frame, text="File prefix (if any)")
        entry_file_prefix = ttk.Entry(frame, textvariable=entry.file_prefix)
        lbl_file_pattern = ttk.Label(frame, text="File pattern, matching site")
        entry_file_pattern = ttk.Entry(frame, textvariable=entry.file_pattern)
        lbl_dst_pattern = ttk.Label(frame, text="Destination pattern")
        entry_dst_pattern = ttk.Entry(frame, textvariable=entry.destination_pattern)
        entry_is_zipped = ttk.Checkbutton(
            frame,
            text="Files are in zip archive",
            variable=entry.is_zipped,
            onvalue=True,
            offvalue=False,
        )

        frame_is_zipped = self.create_custom_distr_frame_is_zipped(frame, entry)

        # Default state
        set_frame_state(frame_is_zipped)(entry_is_zipped.state())

        # Event handlers
        entry_is_zipped.configure(
            command=multi_commands(
                [
                    on_toggle(entry_is_zipped, set_frame_state(frame_is_zipped)),
                    self.refresh,
                ]
            )
        )

        """
        lbl_load_preset.pack(fill="x", expand=False)
        entry_load_preset.pack(fill="x", expand=False, pady=(0, 10))
        """
        lbl_distr_label.pack(fill="x", expand=False)
        entry_distr_label.pack(fill="x", expand=False, pady=(0, 10))
        lbl_src_location.pack(fill="x", expand=False)
        entry_src_location.pack(fill="x", expand=False, pady=(0, 10))
        lbl_file_prefix.pack(fill="x", expand=False)
        entry_file_prefix.pack(fill="x", expand=False, pady=(0, 10))
        lbl_file_pattern.pack(fill="x", expand=False)
        entry_file_pattern.pack(fill="x", expand=False, pady=(0, 10))
        lbl_dst_pattern.pack(fill="x", expand=False)
        entry_dst_pattern.pack(fill="x", expand=False, pady=(0, 10))
        entry_is_zipped.pack(fill="x", expand=False, pady=(0, 10))
        frame_is_zipped.pack(fill="x", expand=False)

        return frame

    def create_custom_distr_frame_is_zipped(
        self, root: ttk.Frame, entry: CustomDistrEntry
    ) -> ttk.Frame:
        frame = ttk.Frame(root)
        lbl_zip_prefix = ttk.Label(frame, text="Zip file prefix (if any)")
        entry_zip_prefix = ttk.Entry(frame, textvariable=entry.zip_prefix)
        lbl_zip_pattern = ttk.Label(frame, text="Zip file pattern")
        entry_zip_pattern = ttk.Entry(frame, textvariable=entry.zip_pattern)

        lbl_zip_prefix.pack(fill="x", expand=False)
        entry_zip_prefix.pack(fill="x", expand=False, pady=(0, 10))
        lbl_zip_pattern.pack(fill="x", expand=False)
        entry_zip_pattern.pack(fill="x", expand=False, pady=(0, 10))

        return frame

    def reset_log_viewer(
        self, max_steps: int = 100
    ) -> tuple[ttk.Progressbar, ttk.Label]:
        if isinstance(self.log_viewer, ttk.Frame):
            self.log_viewer.destroy()

        self.log_viewer = ttk.Frame(self._root, padding=(10,))

        pbar = ttk.Progressbar(
            self.log_viewer,
            orient="horizontal",
            mode="determinate",
            maximum=max_steps,
            value=0,
        )
        label = ttk.Label(self.log_viewer, text="")

        pbar.pack(fill="x", expand=False)
        label.pack(fill="x", expand=False)
        self.log_viewer.pack(fill="x", expand=False)
        return (pbar, label)


# ------------------------------------------------------------------------------
# Tk Helpers
# ------------------------------------------------------------------------------


def create_directory_picker(
    root: ttk.Frame,
    *,
    textvariable: tk.StringVar,
    title: str = "Select a directory",
    refresh: None | Callable[[], bool] = None,
) -> ttk.Frame:
    def _ask() -> None:
        s = filedialog.askdirectory(title=title)
        textvariable.set("" if s is None else os.path.realpath(s))
        if refresh is not None:
            refresh()

    frame = ttk.Frame(root)
    entry = ttk.Entry(frame, textvariable=textvariable)
    button = ttk.Button(frame, text="Browse", command=_ask)

    if refresh is not None:
        entry.configure(
            validate="focusout",
            validatecommand=entry.register(refresh),
        )

    entry.pack(side=tk.LEFT, fill="x", expand=True)
    button.pack(side=tk.LEFT)
    return frame


def multi_commands(commands: list[Callable[[], None]]) -> Callable[[], None]:
    def _multi_commands() -> None:
        for c in commands:
            c()

    return _multi_commands


def on_toggle(cb: ttk.Checkbutton, fn: Callable[[bool], None]) -> Callable[[], None]:
    def _on_toggle() -> None:
        fn("selected" in cb.state())

    return _on_toggle


def set_entry_state(entry: ttk.Entry) -> Callable[[bool], None]:
    def _set_entry_state(enabled: bool) -> None:
        if enabled:
            entry.configure(state=tk.NORMAL)
        else:
            entry.configure(state=tk.DISABLED)

    return _set_entry_state


def set_frame_state(
    frame: tk.Frame | tk.LabelFrame | ttk.Frame | ttk.LabelFrame,
    recurse: bool = False,
) -> Callable[[bool], None]:
    def _set_frame_state(enabled: bool) -> None:
        for child in frame.winfo_children():
            if isinstance(child, (tk.Frame, tk.LabelFrame, ttk.Frame, ttk.LabelFrame)):
                if recurse:
                    set_frame_state(child)(enabled)
            else:
                child.configure(state=tk.NORMAL if enabled else tk.DISABLED)  # type: ignore[call-arg]

    return _set_frame_state
