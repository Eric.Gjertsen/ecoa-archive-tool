from contextlib import contextmanager
from dataclasses import dataclass, asdict, replace
from logging import Logger
from typing import Generator, Any

from ..util.validate import str_field, bool_field, float_field

FIELD_TYPE = "$type"


@dataclass(frozen=True)
class AppEvent:
    def started(self) -> "AppEvent":
        raise NotImplementedError()

    def completed(self) -> "AppEvent":
        raise NotImplementedError()

    def errored(self, error: BaseException) -> "AppEvent":
        raise NotImplementedError()

    def log_warning(self, logger: Logger, msg: str) -> None:
        logger.warning(msg, extra=encode(self))

    def log_exception(self, logger: Logger, error: BaseException) -> None:
        logger.exception(error, extra=encode(self))

    @contextmanager
    def log(self, logger: Logger, msg: str) -> Generator[dict[str, Any], None, None]:
        logger.debug(f"Started: {msg}", extra=encode(self.started()))
        try:
            yield encode(self)
            logger.info(f"Completed: {msg}", extra=encode(self.completed()))
        except Exception as e:
            logger.error(f"Error: {msg}", extra=encode(self.errored(e)))
            raise e


@dataclass(frozen=True)
class Run(AppEvent):
    timestamp: float
    username: str
    client: str
    study: str
    complete: bool = False
    error: BaseException | None = None

    # TODO: add Config

    @classmethod
    def decode(cls, data: dict[str, Any], err: BaseException | None) -> "AppEvent":
        return cls(
            timestamp=float_field("timestamp", data),
            username=str_field("username", data),
            client=str_field("client", data),
            study=str_field("study", data),
            complete=bool_field("complete", data),
            error=err,
        )

    def started(self) -> "AppEvent":
        return replace(self, complete=False, error=None)

    def completed(self) -> "AppEvent":
        return replace(self, complete=True, error=None)

    def errored(self, error: BaseException) -> "AppEvent":
        return replace(self, complete=True, error=error)


@dataclass(frozen=True)
class ProcessSiteCRF(AppEvent):
    client: str
    study: str
    site: str
    src_file: str
    dst_file: str
    complete: bool = False
    error: BaseException | None = None

    @classmethod
    def decode(cls, data: dict[str, Any], err: BaseException | None) -> "AppEvent":
        return cls(
            client=str_field("client", data),
            study=str_field("study", data),
            site=str_field("site", data),
            src_file=str_field("src_file", data),
            dst_file=str_field("dst_file", data),
            complete=bool_field("complete", data),
            error=err,
        )

    def started(self) -> "AppEvent":
        return replace(self, complete=False, error=None)

    def completed(self) -> "AppEvent":
        return replace(self, complete=True, error=None)

    def errored(self, error: BaseException) -> "AppEvent":
        return replace(self, complete=True, error=error)


@dataclass(frozen=True)
class ProcessSiteCRFTOC(AppEvent):
    client: str
    study: str
    site: str
    src_file: str
    dst_file: str
    complete: bool = False
    error: BaseException | None = None

    @classmethod
    def decode(cls, data: dict[str, Any], err: BaseException | None) -> "AppEvent":
        return cls(
            client=str_field("client", data),
            study=str_field("study", data),
            site=str_field("site", data),
            src_file=str_field("src_file", data),
            dst_file=str_field("dst_file", data),
            complete=bool_field("complete", data),
            error=err,
        )

    def started(self) -> "AppEvent":
        return replace(self, complete=False, error=None)

    def completed(self) -> "AppEvent":
        return replace(self, complete=True, error=None)

    def errored(self, error: BaseException) -> "AppEvent":
        return replace(self, complete=True, error=error)


@dataclass(frozen=True)
class ProcessSiteDCRNotMatched(AppEvent):
    client: str
    study: str
    src_file: str

    @classmethod
    def decode(cls, data: dict[str, Any]) -> "AppEvent":
        return cls(
            client=str_field("client", data),
            study=str_field("study", data),
            src_file=str_field("src_file", data),
        )

    def started(self) -> "AppEvent":
        return self

    def completed(self) -> "AppEvent":
        return self

    def errored(self, error: BaseException) -> "AppEvent":
        return self


@dataclass(frozen=True)
class ProcessSiteDCR(AppEvent):
    client: str
    study: str
    site: str
    src_file: str
    dst_file: str
    complete: bool = False
    error: BaseException | None = None

    @classmethod
    def decode(cls, data: dict[str, Any], err: BaseException | None) -> "AppEvent":
        return cls(
            client=str_field("client", data),
            study=str_field("study", data),
            site=str_field("site", data),
            src_file=str_field("src_file", data),
            dst_file=str_field("dst_file", data),
            complete=bool_field("complete", data),
            error=err,
        )

    def started(self) -> "AppEvent":
        return replace(self, complete=False, error=None)

    def completed(self) -> "AppEvent":
        return replace(self, complete=True, error=None)

    def errored(self, error: BaseException) -> "AppEvent":
        return replace(self, complete=True, error=error)


@dataclass(frozen=True)
class ProcessSiteDATA(AppEvent):
    client: str
    study: str
    site: str
    src_file: str
    dst_file: str
    complete: bool = False
    error: BaseException | None = None

    @classmethod
    def decode(cls, data: dict[str, Any], err: BaseException | None) -> "AppEvent":
        return cls(
            client=str_field("client", data),
            study=str_field("study", data),
            site=str_field("site", data),
            src_file=str_field("src_file", data),
            dst_file=str_field("dst_file", data),
            complete=bool_field("complete", data),
            error=err,
        )

    def started(self) -> "AppEvent":
        return replace(self, complete=False, error=None)

    def completed(self) -> "AppEvent":
        return replace(self, complete=True, error=None)

    def errored(self, error: BaseException) -> "AppEvent":
        return replace(self, complete=True, error=error)


@dataclass(frozen=True)
class ProcessSiteDOC(AppEvent):
    client: str
    study: str
    site: str
    src_file: str
    dst_file: str
    complete: bool = False
    error: BaseException | None = None

    @classmethod
    def decode(cls, data: dict[str, Any], err: BaseException | None) -> "AppEvent":
        return cls(
            client=str_field("client", data),
            study=str_field("study", data),
            site=str_field("site", data),
            src_file=str_field("src_file", data),
            dst_file=str_field("dst_file", data),
            complete=bool_field("complete", data),
            error=err,
        )

    def started(self) -> "AppEvent":
        return replace(self, complete=False, error=None)

    def completed(self) -> "AppEvent":
        return replace(self, complete=True, error=None)

    def errored(self, error: BaseException) -> "AppEvent":
        return replace(self, complete=True, error=error)


@dataclass(frozen=True)
class ProcessSitePDF(AppEvent):
    client: str
    study: str
    site: str
    src_file: str
    dst_file: str
    complete: bool = False
    error: BaseException | None = None

    @classmethod
    def decode(cls, data: dict[str, Any], err: BaseException | None) -> "AppEvent":
        return cls(
            client=str_field("client", data),
            study=str_field("study", data),
            site=str_field("site", data),
            src_file=str_field("src_file", data),
            dst_file=str_field("dst_file", data),
            complete=bool_field("complete", data),
            error=err,
        )

    def started(self) -> "AppEvent":
        return replace(self, complete=False, error=None)

    def completed(self) -> "AppEvent":
        return replace(self, complete=True, error=None)

    def errored(self, error: BaseException) -> "AppEvent":
        return replace(self, complete=True, error=error)


@dataclass(frozen=True)
class ProcessSiteDistrNotMatched(AppEvent):
    distr_label: str
    client: str
    study: str
    src_file: str

    @classmethod
    def decode(cls, data: dict[str, Any]) -> "AppEvent":
        return cls(
            distr_label=str_field("distr_label", data),
            client=str_field("client", data),
            study=str_field("study", data),
            src_file=str_field("src_file", data),
        )

    def started(self) -> "AppEvent":
        return self

    def completed(self) -> "AppEvent":
        return self

    def errored(self, error: BaseException) -> "AppEvent":
        return self


@dataclass(frozen=True)
class ProcessSiteDistr(AppEvent):
    distr_label: str
    client: str
    study: str
    site: str
    src_file: str
    dst_file: str
    complete: bool = False
    error: BaseException | None = None

    @classmethod
    def decode(cls, data: dict[str, Any], err: BaseException | None) -> "AppEvent":
        return cls(
            distr_label=str_field("distr_label", data),
            client=str_field("client", data),
            study=str_field("study", data),
            site=str_field("site", data),
            src_file=str_field("src_file", data),
            dst_file=str_field("dst_file", data),
            complete=bool_field("complete", data),
            error=err,
        )

    def started(self) -> "AppEvent":
        return replace(self, complete=False, error=None)

    def completed(self) -> "AppEvent":
        return replace(self, complete=True, error=None)

    def errored(self, error: BaseException) -> "AppEvent":
        return replace(self, complete=True, error=error)


@dataclass(frozen=True)
class MoveAttachmentsCopyReport(AppEvent):
    client: str
    study: str
    src_file: str
    dst_file: str
    complete: bool = False
    error: BaseException | None = None

    @classmethod
    def decode(cls, data: dict[str, Any], err: BaseException | None) -> "AppEvent":
        return cls(
            client=str_field("client", data),
            study=str_field("study", data),
            src_file=str_field("src_file", data),
            dst_file=str_field("dst_file", data),
            complete=bool_field("complete", data),
            error=err,
        )

    def started(self) -> "AppEvent":
        return replace(self, complete=False, error=None)

    def completed(self) -> "AppEvent":
        return replace(self, complete=True, error=None)

    def errored(self, error: BaseException) -> "AppEvent":
        return replace(self, complete=True, error=error)


@dataclass(frozen=True)
class MoveAttachmentsCopy(AppEvent):
    client: str
    study: str
    src_dir: str
    dst_dir: str
    complete: bool = False
    error: BaseException | None = None

    @classmethod
    def decode(cls, data: dict[str, Any], err: BaseException | None) -> "AppEvent":
        return cls(
            client=str_field("client", data),
            study=str_field("study", data),
            src_dir=str_field("src_dir", data),
            dst_dir=str_field("dst_dir", data),
            complete=bool_field("complete", data),
            error=err,
        )

    def started(self) -> "AppEvent":
        return replace(self, complete=False, error=None)

    def completed(self) -> "AppEvent":
        return replace(self, complete=True, error=None)

    def errored(self, error: BaseException) -> "AppEvent":
        return replace(self, complete=True, error=error)


@dataclass(frozen=True)
class MoveAttachmentsMove(AppEvent):
    client: str
    study: str
    site: str
    src_file: str
    dst_file: str
    complete: bool = False
    error: BaseException | None = None

    @classmethod
    def decode(cls, data: dict[str, Any], err: BaseException | None) -> "AppEvent":
        return cls(
            client=str_field("client", data),
            study=str_field("study", data),
            site=str_field("site", data),
            src_file=str_field("src_file", data),
            dst_file=str_field("dst_file", data),
            complete=bool_field("complete", data),
            error=err,
        )

    def started(self) -> "AppEvent":
        return replace(self, complete=False, error=None)

    def completed(self) -> "AppEvent":
        return replace(self, complete=True, error=None)

    def errored(self, error: BaseException) -> "AppEvent":
        return replace(self, complete=True, error=error)


@dataclass(frozen=True)
class MoveAttachmentsNotInReport(AppEvent):
    client: str
    study: str
    src_file: str

    @classmethod
    def decode(cls, data: dict[str, Any]) -> "AppEvent":
        return cls(
            client=str_field("client", data),
            study=str_field("study", data),
            src_file=str_field("src_file", data),
        )

    def started(self) -> "AppEvent":
        return self

    def completed(self) -> "AppEvent":
        return self

    def errored(self, error: BaseException) -> "AppEvent":
        return self


@dataclass(frozen=True)
class MoveAttachmentsNotInSource(AppEvent):
    client: str
    study: str
    dcf_id: str
    repository_id: str
    file_name: str
    site: str

    @classmethod
    def decode(cls, data: dict[str, Any]) -> "AppEvent":
        return cls(
            client=str_field("client", data),
            study=str_field("study", data),
            dcf_id=str_field("dcf_id", data),
            repository_id=str_field("repository_id", data),
            file_name=str_field("file_name", data),
            site=str_field("site", data),
        )

    def started(self) -> "AppEvent":
        return self

    def completed(self) -> "AppEvent":
        return self

    def errored(self, error: BaseException) -> "AppEvent":
        return self


def decode(
    data: dict[str, Any],
    err: BaseException | None,
) -> AppEvent:
    t = str_field(FIELD_TYPE, data)
    return decode_app_event_with_type_and_error(t, err, data)


def decode_app_event_with_type_and_error(
    t: str, err: BaseException | None, data: dict[str, Any]
) -> AppEvent:
    match t:
        case "Run":
            return Run.decode(data, err)
        case "ProcessSiteCRF":
            return ProcessSiteCRF.decode(data, err)
        case "ProcessSiteCRFTOC":
            return ProcessSiteCRFTOC.decode(data, err)
        case "ProcessSiteDCRNotMatched":
            return ProcessSiteDCRNotMatched.decode(data)
        case "ProcessSiteDCR":
            return ProcessSiteDCR.decode(data, err)
        case "ProcessSiteDATA":
            return ProcessSiteDATA.decode(data, err)
        case "ProcessSiteDOC":
            return ProcessSiteDOC.decode(data, err)
        case "ProcessSitePDF":
            return ProcessSitePDF.decode(data, err)
        case "ProcessSiteDistr":
            return ProcessSiteDistr.decode(data, err)
        case "MoveAttachmentsCopyReport":
            return MoveAttachmentsCopyReport.decode(data, err)
        case "MoveAttachmentsCopy":
            return MoveAttachmentsCopy.decode(data, err)
        case "MoveAttachmentsMove":
            return MoveAttachmentsMove.decode(data, err)
        case "MoveAttachmentsNotInReport":
            return MoveAttachmentsNotInReport.decode(data)
        case "MoveAttachmentsNotInSource":
            return MoveAttachmentsNotInSource.decode(data)
        case _:
            raise ValueError(f"Unknown AppEvent type '{t}'")


def encode(event: AppEvent) -> dict[str, Any]:
    t = event.__class__.__name__
    data = asdict(event)
    data[FIELD_TYPE] = t
    data["stage"] = t
    return data
