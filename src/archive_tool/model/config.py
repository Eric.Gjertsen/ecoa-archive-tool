from dataclasses import dataclass
from datetime import datetime
import os.path
import re
from typing import Any


@dataclass(frozen=True)
class ProcessSitesSourceDirConventions:
    data_dir: str
    dcr_dir: str
    site_excludes: list[str]
    crf_dir_pattern: str
    crf_toc_pattern: str
    dcr_match_pattern: str
    data_dir_pattern: str
    docs_dir_pattern: str
    pdfs_dir_pattern: str

    def data_path(self, root_path: str) -> str:
        return os.path.join(root_path, self.data_dir)

    def dcr_path(self, root_path: str) -> str:
        return os.path.join(root_path, self.dcr_dir)

    def site_data_path(self, root_path: str, site: str) -> str:
        return os.path.join(root_path, self.data_dir_pattern.format(site=site))

    def crf_path(self, root_path: str, site: str) -> str:
        return os.path.join(root_path, self.crf_dir_pattern.format(site=site))

    def crf_toc_path(self, root_path: str, site: str) -> str:
        return os.path.join(root_path, self.crf_toc_pattern.format(site=site))

    def docs_path(self, root_path: str, site: str) -> str:
        return os.path.join(root_path, self.docs_dir_pattern.format(site=site))

    def pdfs_path(self, root_path: str, site: str) -> str:
        return os.path.join(root_path, self.pdfs_dir_pattern.format(site=site))

    def to_dict(self) -> dict[str, Any]:
        return {
            "data_dir": self.data_dir,
            "dcr_dir": self.dcr_dir,
            "site_excludes": self.site_excludes,
            "crf_dir_pattern": self.crf_dir_pattern,
            "crf_toc_pattern": self.crf_toc_pattern,
            "dcr_match_pattern": self.dcr_match_pattern,
            "data_dir_pattern": self.data_dir_pattern,
            "docs_dir_pattern": self.docs_dir_pattern,
            "pdfs_dir_pattern": self.pdfs_dir_pattern,
        }


@dataclass(frozen=True)
class ProcessSitesDestDirConventions:
    crf_dir_pattern: str
    crf_toc_pattern: str
    dcr_dir_pattern: str
    data_dir_pattern: str
    docs_dir_pattern: str
    pdfs_dir_pattern: str

    def site_data_path(self, root_path: str, site: str) -> str:
        return os.path.join(root_path, self.data_dir_pattern.format(site=site))

    def crf_path(self, root_path: str, site: str) -> str:
        return os.path.join(root_path, self.crf_dir_pattern.format(site=site))

    def crf_toc_path(self, root_path: str, site: str) -> str:
        return os.path.join(root_path, self.crf_toc_pattern.format(site=site))

    def dcr_path(self, root_path: str, site: str) -> str:
        return os.path.join(root_path, self.dcr_dir_pattern.format(site=site))

    def docs_path(self, root_path: str, site: str) -> str:
        return os.path.join(root_path, self.docs_dir_pattern.format(site=site))

    def pdfs_path(self, root_path: str, site: str) -> str:
        return os.path.join(root_path, self.pdfs_dir_pattern.format(site=site))

    def to_dict(self) -> dict[str, Any]:
        return {
            "crf_dir_pattern": self.crf_dir_pattern,
            "crf_toc_pattern": self.crf_toc_pattern,
            "dcr_dir_pattern": self.dcr_dir_pattern,
            "data_dir_pattern": self.data_dir_pattern,
            "docs_dir_pattern": self.docs_dir_pattern,
            "pdfs_dir_pattern": self.pdfs_dir_pattern,
        }


@dataclass(frozen=True)
class DistrConfig:
    label: str
    source_location: str
    prefix: str
    pattern: str
    destination_pattern: str
    is_zipped: bool
    zip_prefix: str
    zip_pattern: str

    @property
    def distr_file_pattern(self) -> re.Pattern[str]:
        return re.compile(self.pattern, re.I)

    @property
    def distr_zip_pattern(self) -> re.Pattern[str]:
        return re.compile(self.zip_pattern, re.I)

    def source_path(self, root: str) -> str:
        return os.path.join(root, self.source_location)

    def destination_path(self, root: str, site: str) -> str:
        return os.path.join(root, self.destination_pattern.format(site=site))

    def to_dict(self) -> dict[str, Any]:
        return {
            "label": self.label,
            "source_location": self.source_location,
            "prefix": self.prefix,
            "pattern": self.pattern,
            "destination_pattern": self.destination_pattern,
            "is_zipped": self.is_zipped,
            "zip_prefix": self.zip_prefix,
            "zip_pattern": self.zip_pattern,
        }


@dataclass(frozen=True)
class ProcessSitesConfig:
    dcr_prefix: str
    source_location: str
    source_conventions: ProcessSitesSourceDirConventions
    destination_conventions: ProcessSitesDestDirConventions
    custom_distr: DistrConfig | None

    @property
    def source_data_path(self) -> str:
        return self.source_conventions.data_path(self.source_location)

    @property
    def source_site_excludes(self) -> set[str]:
        return {d.lower() for d in self.source_conventions.site_excludes}

    @property
    def source_dcr_path(self) -> str:
        return self.source_conventions.dcr_path(self.source_location)

    @property
    def source_dcr_match_pattern(self) -> re.Pattern[str]:
        return re.compile(self.source_conventions.dcr_match_pattern, re.I)

    @property
    def source_custom_distr_path(self) -> str | None:
        if self.custom_distr is None:
            return None
        return self.custom_distr.source_path(self.source_location)

    def source_site_data_path(self, site: str) -> str:
        return self.source_conventions.site_data_path(self.source_location, site)

    def source_crf_path(self, site: str) -> str:
        return self.source_conventions.crf_path(self.source_location, site)

    def source_crf_toc_path(self, site: str) -> str:
        return self.source_conventions.crf_toc_path(self.source_location, site)

    def source_docs_path(self, site: str) -> str:
        return self.source_conventions.docs_path(self.source_location, site)

    def source_pdfs_path(self, destination_location: str, site: str) -> str:
        """
        Please note this oddness: the source for the PDFs is in the destination
        location, but uses the source conventions
        """
        return self.source_conventions.pdfs_path(destination_location, site)

    def destination_site_data_path(self, destination_location: str, site: str) -> str:
        return self.destination_conventions.site_data_path(destination_location, site)

    def destination_crf_path(self, destination_location: str, site: str) -> str:
        return self.destination_conventions.crf_path(destination_location, site)

    def destination_crf_toc_path(self, destination_location: str, site: str) -> str:
        return self.destination_conventions.crf_toc_path(destination_location, site)

    def destination_dcr_path(self, destination_location: str, site: str) -> str:
        return self.destination_conventions.dcr_path(destination_location, site)

    def destination_docs_path(self, destination_location: str, site: str) -> str:
        return self.destination_conventions.docs_path(destination_location, site)

    def destination_pdfs_path(self, destination_location: str, site: str) -> str:
        return self.destination_conventions.pdfs_path(destination_location, site)

    def destination_custom_distr_path(
        self, destination_location: str, site: str
    ) -> str | None:
        if self.custom_distr is None:
            return None
        return self.custom_distr.destination_path(destination_location, site)

    def to_default(self) -> "DefaultProcessSitesConfig":
        return DefaultProcessSitesConfig(
            source_conventions=self.source_conventions,
            destination_conventions=self.destination_conventions,
        )

    def to_dict(self) -> dict[str, Any]:
        d = {
            "dcr_prefix": self.dcr_prefix,
            "source_location": self.source_location,
            "source_conventions": self.source_conventions.to_dict(),
            "destination_conventions": self.destination_conventions.to_dict(),
        }
        if self.custom_distr is not None:
            d["custom_distr"] = self.custom_distr.to_dict()
        return d


@dataclass(frozen=True)
class MoveAttachmentsSourceDirConventions:
    attachments_dir: str

    def attachments_path(self, root_path: str) -> str:
        return os.path.join(root_path, self.attachments_dir)

    def to_dict(self) -> dict[str, Any]:
        return {
            "attachments_dir": self.attachments_dir,
        }


@dataclass(frozen=True)
class MoveAttachmentsDestDirConventions:
    work_dir: str
    dir_pattern: str

    def work_path(self, root_path: str) -> str:
        return os.path.join(root_path, self.work_dir)

    def attachments_path(self, root_path: str, site: str) -> str:
        return os.path.join(root_path, self.dir_pattern.format(site=site))

    def to_dict(self) -> dict[str, Any]:
        return {
            "work_dir": self.work_dir,
            "dir_pattern": self.dir_pattern,
        }


@dataclass(frozen=True)
class MoveAttachmentsConfig:
    copy_from_source: bool
    copy_report_from_source: bool
    attachments_report: str
    source_location: str
    report_source_location: str
    source_conventions: MoveAttachmentsSourceDirConventions
    destination_conventions: MoveAttachmentsDestDirConventions

    @property
    def source_attachments_path(self) -> str:
        return self.source_conventions.attachments_path(self.source_location)

    @property
    def source_attachments_report_path(self) -> str:
        return os.path.join(self.report_source_location, self.attachments_report)

    def destination_attachments_report_path(self, destination_location: str) -> str:
        return os.path.join(destination_location, self.attachments_report)

    def destination_work_path(self, destination_location: str) -> str:
        return self.destination_conventions.work_path(destination_location)

    def destination_attachments_path(self, destination_location: str, site: str) -> str:
        return self.destination_conventions.attachments_path(
            destination_location,
            site,
        )

    def to_default(self) -> "DefaultMoveAttachmentsConfig":
        return DefaultMoveAttachmentsConfig(
            copy_from_source=self.copy_from_source,
            copy_report_from_source=self.copy_report_from_source,
            source_conventions=self.source_conventions,
            destination_conventions=self.destination_conventions,
        )

    def to_dict(self) -> dict[str, Any]:
        return {
            "copy_from_source": self.copy_from_source,
            "copy_report_from_source": self.copy_report_from_source,
            "attachments_report": self.attachments_report,
            "source_location": self.source_location,
            "report_source_location": self.report_source_location,
            "source_conventions": self.source_conventions.to_dict(),
            "destination_conventions": self.destination_conventions.to_dict(),
        }


@dataclass(frozen=True)
class ReportConventions:
    log_pattern: str
    db_pattern: str
    report_pattern: str

    def log_file_path(
        self, root_path: str, *, client: str, study: str, timestamp: datetime
    ) -> str:
        return os.path.join(
            root_path,
            self.log_pattern.format(client=client, study=study, timestamp=timestamp),
        )

    def db_file_path(
        self, root_path: str, *, client: str, study: str, timestamp: datetime
    ) -> str:
        return os.path.join(
            root_path,
            self.db_pattern.format(client=client, study=study, timestamp=timestamp),
        )

    def report_file_path(
        self, root_path: str, *, client: str, study: str, timestamp: datetime
    ) -> str:
        return os.path.join(
            root_path,
            self.report_pattern.format(client=client, study=study, timestamp=timestamp),
        )

    def to_dict(self) -> dict[str, Any]:
        return {
            "log_pattern": self.log_pattern,
            "db_pattern": self.db_pattern,
            "report_pattern": self.report_pattern,
        }


@dataclass(frozen=True)
class Config:
    client: str
    study: str
    overwrite: bool
    skip_if_exists: bool
    fail_fast: bool
    destination_location: str
    report: ReportConventions
    process_sites: ProcessSitesConfig
    move_attachments: MoveAttachmentsConfig

    def to_default(self) -> "DefaultConfig":
        return DefaultConfig(
            overwrite=self.overwrite,
            skip_if_exists=self.skip_if_exists,
            fail_fast=self.fail_fast,
            report=self.report,
            process_sites=self.process_sites.to_default(),
            move_attachments=self.move_attachments.to_default(),
        )

    def to_dict(self) -> dict[str, Any]:
        return {
            "client": self.client,
            "study": self.study,
            "overwrite": self.overwrite,
            "skip_if_exists": self.skip_if_exists,
            "fail_fast": self.fail_fast,
            "destination_location": self.destination_location,
            "report": self.report.to_dict(),
            "process_sites": self.process_sites.to_dict(),
            "move_attachments": self.move_attachments.to_dict(),
        }


# ------------------------------------------------------------------------------
# DefaultConfig
# ------------------------------------------------------------------------------


@dataclass(frozen=True)
class DefaultProcessSitesConfig:
    source_conventions: ProcessSitesSourceDirConventions
    destination_conventions: ProcessSitesDestDirConventions

    def to_dict(self) -> dict[str, Any]:
        return {
            "source_conventions": self.source_conventions.to_dict(),
            "destination_conventions": self.destination_conventions.to_dict(),
        }


@dataclass(frozen=True)
class DefaultMoveAttachmentsConfig:
    copy_from_source: bool
    copy_report_from_source: bool
    source_conventions: MoveAttachmentsSourceDirConventions
    destination_conventions: MoveAttachmentsDestDirConventions

    def to_dict(self) -> dict[str, Any]:
        return {
            "copy_from_source": self.copy_from_source,
            "copy_report_from_source": self.copy_report_from_source,
            "source_conventions": self.source_conventions.to_dict(),
            "destination_conventions": self.destination_conventions.to_dict(),
        }


@dataclass(frozen=True)
class DefaultConfig:
    overwrite: bool
    skip_if_exists: bool
    fail_fast: bool
    report: ReportConventions
    process_sites: DefaultProcessSitesConfig
    move_attachments: DefaultMoveAttachmentsConfig

    def to_dict(self) -> dict[str, Any]:
        return {
            "overwrite": self.overwrite,
            "skip_if_exists": self.skip_if_exists,
            "fail_fast": self.fail_fast,
            "report": self.report.to_dict(),
            "process_sites": self.process_sites.to_dict(),
            "move_attachments": self.move_attachments.to_dict(),
        }
