import os.path


class AppError(Exception):
    pass


class SourceDirDoesNotExistError(AppError):
    def __init__(self, src: str, label: str | None = None):
        self.src = src
        self.label = label

    def __str__(self) -> str:
        if self.label is None:
            return f"Source directory does not exist: {self.src}."
        else:
            return f"{self.label} source directory does not exist: {self.src}."


class SourceFileDoesNotExistError(AppError):
    def __init__(self, src: str):
        self.src = src

    def __str__(self) -> str:
        return f"Source file does not exist: {self.src}."


class DestinationFileExistsError(AppError):
    def __init__(self, src: str, dest: str):
        self.src = src
        self.dest = dest

    def __str__(self) -> str:
        return (
            f"Destination file {self.dest} already exists. "
            f"Cannot overwrite with copy of source file {self.src}."
        )


class SourceFileExistsInDestinationError(AppError):
    def __init__(self, src: str, dest: str):
        self.src = src
        self.dest = dest

    @property
    def base_src(self) -> str:
        return os.path.basename(self.src)

    def __str__(self) -> str:
        return (
            f"Source file {self.base_src} already exists in destination {self.dest}. "
            f"Cannot overwrite with copy of source file {self.src}."
        )


class MatchPatternError(AppError):
    def __init__(self, label: str):
        self.label = label

    def __str__(self) -> str:
        return (
            "Unparseable {self.label} match pattern in config. "
            "The syntax is python-flavored regular expression. "
            "Please check and try again."
        )
