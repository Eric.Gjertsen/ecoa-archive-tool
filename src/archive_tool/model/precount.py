from dataclasses import dataclass


@dataclass
class Precount:
    crf: int = 0
    crf_toc: int = 0
    data: int = 0
    docs: int = 0
    pdfs: int = 0
    dcrs: int = 0
    attachments: int = 0

    def __add__(self, other: "Precount") -> "Precount":
        return Precount(
            self.crf + other.crf,
            self.crf_toc + other.crf_toc,
            self.data + other.data,
            self.docs + other.docs,
            self.pdfs + other.pdfs,
            self.dcrs + other.dcrs,
            self.attachments + other.attachments,
        )

    def total(
        self, *, copy_report_from_source: bool = False, copy_from_source: bool = False
    ) -> int:
        return (
            self.crf
            + self.crf_toc
            + self.data
            + self.docs
            + self.pdfs
            + self.dcrs
            + (1 if copy_report_from_source else 0)
            + (self.attachments if copy_from_source else 0)
            + self.attachments
        )
