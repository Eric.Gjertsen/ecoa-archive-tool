from filecmp import dircmp
import os.path
import os
import shutil

import pytest
from _pytest.fixtures import FixtureRequest

from archive_tool.__main__ import main
from archive_tool.model.config import (
    ProcessSitesSourceDirConventions,
    ProcessSitesDestDirConventions,
    MoveAttachmentsSourceDirConventions,
    MoveAttachmentsDestDirConventions,
)

from util import TestFilesGenerator

# FIXTURES


@pytest.fixture
def process_sites_src_conventions() -> ProcessSitesSourceDirConventions:
    return ProcessSitesSourceDirConventions(
        data_dir="DataDir",
        dcr_dir="DCR",
        site_excludes=["DCF", "Device Data", "DOCS"],
        crf_dir_pattern=r"CRF\{site}",
        crf_toc_pattern=r"CRF\crftoc_{site}.pdf",
        dcr_match_pattern=r"_(.+)_.+\.pdf",
        data_dir_pattern=r"DataDir\{site}",
        docs_dir_pattern=r"site histories\{site}",
        pdfs_dir_pattern=r"Sites\All Sites",
    )


@pytest.fixture
def process_sites_dst_conventions() -> ProcessSitesDestDirConventions:
    return ProcessSitesDestDirConventions(
        crf_dir_pattern=r"Sites\{site}\CRF\{site}",
        crf_toc_pattern=r"Sites\{site}\CRF\crftoc.pdf",
        dcr_dir_pattern=r"Sites\{site}\DCR",
        data_dir_pattern=r"Sites\{site}\XML Data Directory",
        docs_dir_pattern=r"Sites\{site}\Documents",
        pdfs_dir_pattern=r"Sites\{site}\pdf",
    )


@pytest.fixture
def move_attachments_src_conventions() -> MoveAttachmentsSourceDirConventions:
    return MoveAttachmentsSourceDirConventions(attachments_dir=r"DCR\Attachments")


@pytest.fixture
def move_attachments_dst_conventions() -> MoveAttachmentsDestDirConventions:
    return MoveAttachmentsDestDirConventions(
        work_dir=r"Working Folder - Archive\Attachments",
        dir_pattern=r"Sites\{site}\DCR\Attachments",
    )


@pytest.fixture
def client() -> str:
    return "CLIENT"


@pytest.fixture
def study() -> str:
    return "STUDY"


@pytest.fixture
def dcr_prefix(client: str, study: str) -> str:
    return f"{client}_{study}"


@pytest.fixture
def sites() -> set[str]:
    return {"001", "002", "003", "004", "005"}


@pytest.fixture
def pdfs() -> set[str]:
    return {"a.pdf", "b.pdf", "c.pdf"}


@pytest.fixture
def attachments_report() -> str:
    return "all_attachments.rpt"


@pytest.fixture
def config_file_name() -> str:
    return "archive_tool.ini"


# TESTS


def test_main_success(
    request: FixtureRequest,
    client: str,
    study: str,
    process_sites_src_conventions: ProcessSitesSourceDirConventions,
    process_sites_dst_conventions: ProcessSitesDestDirConventions,
    move_attachments_src_conventions: MoveAttachmentsSourceDirConventions,
    move_attachments_dst_conventions: MoveAttachmentsDestDirConventions,
    dcr_prefix: str,
    sites: set[str],
    pdfs: set[str],
    attachments_report: str,
    config_file_name: str,
) -> None:
    name = request.node.name
    dest = destination_location(name, client, study)
    config = os.path.join(dest, config_file_name)
    reset_destination(name)

    gen = TestFilesGenerator(
        client=client,
        study=study,
        source=source_location(name, client, study),
        attachments_source=attachments_source_location(name, client, study),
        expected=expected_location(name, client, study),
        src_conventions=process_sites_src_conventions,
        dst_conventions=process_sites_dst_conventions,
        attachments_src_conventions=move_attachments_src_conventions,
        attachments_dst_conventions=move_attachments_dst_conventions,
        dcr_prefix=dcr_prefix,
        sites=sites,
        pdfs=pdfs,
        attachments_report=attachments_report,
    )
    gen.generate()
    gen.create_destination(dest)
    gen.generate_config_file(
        config_file_name,
        destination=dest,
    )

    main(["-c", config])

    assert_directory_matches(
        destination_root_path(name),
        expected_root_path(name),
    )


# ASSERTIONS


def assert_directory_matches(act: str, exp: str) -> None:
    c = dircmp(act, exp)
    act_extra = c.left_only
    assert len(act_extra) == 0, "Unexpected files:\n" + "\n".join(
        ["  " + f for f in act_extra]
    )
    exp_extra = c.right_only
    assert len(exp_extra) == 0, "Missing files:\n" + "\n".join(
        ["  " + f for f in exp_extra]
    )
    diff_files = c.diff_files
    assert len(diff_files) == 0, "Different file contents:\n" + "\n".join(
        ["  " + f for f in diff_files]
    )


# HELPERS


def source_location(name: str, client: str, study: str) -> str:
    return os.path.join(source_root_path(name), f"{client}_{study}")


def attachments_source_location(name: str, client: str, study: str) -> str:
    return os.path.join(attachments_source_root_path(name), f"{client}_{study}")


def expected_location(name: str, client: str, study: str) -> str:
    return os.path.join(expected_root_path(name), f"{client}\\{study}")


def destination_location(name: str, client: str, study: str) -> str:
    return os.path.join(destination_root_path(name), f"{client}\\{study}")


def output_root_path() -> str:
    return os.path.join("test", "output", "test_main")


def source_root_path(name: str) -> str:
    return os.path.join(output_root_path(), name, "source")


def attachments_source_root_path(name: str) -> str:
    return os.path.join(output_root_path(), name, "attachments_source")


def expected_root_path(name: str) -> str:
    return os.path.join(output_root_path(), name, "expected")


def destination_root_path(name: str) -> str:
    return os.path.join(output_root_path(), name, "destination")


def config_file_path(name: str) -> str:
    return os.path.join(destination_root_path(name), "archive_tool.ini")


def reset_destination(name: str) -> None:
    reset_path(destination_root_path(name))


def reset_path(path: str) -> None:
    rm_fr(path)
    mkdir_p(path)


def rm_fr(path: str) -> None:
    if not os.path.exists(path):
        return
    if os.path.relpath(path, ".").startswith("."):
        raise ValueError(f"Sorry Dave, I can't remove {path}.")
    shutil.rmtree(path)


def mkdir_p(path: str) -> None:
    os.makedirs(path, exist_ok=True)
