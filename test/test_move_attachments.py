import csv
import logging
import os.path
import os
import re
import shutil

import pytest
from _pytest.fixtures import FixtureRequest
from _pytest.logging import LogCaptureFixture

from archive_tool.__main__ import move_attachments
from archive_tool.adapter.attachments_db import FIELD_SITE
from archive_tool.model.config import (
    MoveAttachmentsConfig,
    MoveAttachmentsSourceDirConventions,
    MoveAttachmentsDestDirConventions,
)

logging.basicConfig(level=logging.DEBUG)


# FIXTURES


@pytest.fixture
def source_conventions() -> MoveAttachmentsSourceDirConventions:
    return MoveAttachmentsSourceDirConventions(attachments_dir=r"DCR\Attachments")


@pytest.fixture
def destination_conventions() -> MoveAttachmentsDestDirConventions:
    return MoveAttachmentsDestDirConventions(
        work_dir=r"Working Folder - Archive\Attachments",
        dir_pattern=r"Sites\{site}\DCR\Attachments",
    )


@pytest.fixture
def client() -> str:
    return "CLIENT"


@pytest.fixture
def study() -> str:
    return "STUDY"


@pytest.fixture
def attachments_report() -> str:
    return "attachments.rpt"


def test_all_match_types(
    request: FixtureRequest,
    source_conventions: MoveAttachmentsSourceDirConventions,
    destination_conventions: MoveAttachmentsDestDirConventions,
    client: str,
    study: str,
    attachments_report: str,
) -> None:
    name = request.node.name
    reset_path(destination_root_path(name))

    config = MoveAttachmentsConfig(
        copy_from_source=True,
        copy_report_from_source=True,
        attachments_report=attachments_report,
        source_location=source_location(name, client, study),
        report_source_location=report_source_location(name, client, study),
        source_conventions=source_conventions,
        destination_conventions=destination_conventions,
    )
    dst = destination_location(name, client, study)

    move_attachments(
        config,
        dst,
        client=client,
        study=study,
        overwrite=True,
        skip_if_exists=False,
        fail_fast=True,
    )

    assert_working_dir_empty(config, dst)
    assert_attachments_in_each_site(config, dst)


def test_not_in_report(
    request: FixtureRequest,
    source_conventions: MoveAttachmentsSourceDirConventions,
    destination_conventions: MoveAttachmentsDestDirConventions,
    client: str,
    study: str,
    attachments_report: str,
    caplog: LogCaptureFixture,
) -> None:
    name = request.node.name
    reset_path(destination_root_path(name))

    config = MoveAttachmentsConfig(
        copy_from_source=True,
        copy_report_from_source=True,
        attachments_report=attachments_report,
        source_location=source_location(name, client, study),
        report_source_location=report_source_location(name, client, study),
        source_conventions=source_conventions,
        destination_conventions=destination_conventions,
    )
    dst = destination_location(name, client, study)

    move_attachments(
        config,
        dst,
        client=client,
        study=study,
        overwrite=True,
        skip_if_exists=False,
        fail_fast=True,
    )

    assert_working_dir_has_files(config, dst, ["not_in_report.pdf"])
    assert_not_in_report_warnings_in_log(caplog, ["not_in_report.pdf"])


def test_not_in_source(
    request: FixtureRequest,
    source_conventions: MoveAttachmentsSourceDirConventions,
    destination_conventions: MoveAttachmentsDestDirConventions,
    client: str,
    study: str,
    attachments_report: str,
    caplog: LogCaptureFixture,
) -> None:
    name = request.node.name
    reset_path(destination_root_path(name))

    config = MoveAttachmentsConfig(
        copy_from_source=True,
        copy_report_from_source=True,
        attachments_report=attachments_report,
        source_location=source_location(name, client, study),
        report_source_location=report_source_location(name, client, study),
        source_conventions=source_conventions,
        destination_conventions=destination_conventions,
    )
    dst = destination_location(name, client, study)

    move_attachments(
        config,
        dst,
        client=client,
        study=study,
        overwrite=True,
        skip_if_exists=False,
        fail_fast=True,
    )

    assert_working_dir_empty(config, dst)
    assert_not_in_source_warnings_in_log(caplog, ["not_in_source.pdf"])


# ASSERTIONS


def assert_working_dir_empty(config: MoveAttachmentsConfig, dst: str) -> None:
    work_path = config.destination_work_path(dst)
    files = list(os.listdir(work_path))
    act = len(files)
    assert (
        act == 0
    ), f"Expected no files left in working dir {work_path}, but there were {act}"


def assert_working_dir_has_files(
    config: MoveAttachmentsConfig, dst: str, exp_files: list[str]
) -> None:
    work_path = config.destination_work_path(dst)
    act_files = set(os.listdir(work_path))
    for exp_file in exp_files:
        assert (
            exp_file in act_files
        ), f"Expected file {exp_file} still in working dir {work_path}, but not there"


def assert_attachments_in_each_site(config: MoveAttachmentsConfig, dst: str) -> None:
    attachments_rpt_path = config.destination_attachments_report_path(dst)
    assert os.path.exists(attachments_rpt_path)
    with open(attachments_rpt_path, "r", newline="") as f:
        reader = csv.DictReader(f, delimiter="|")
        i = 0
        for row in reader:
            sitedir = config.destination_attachments_path(dst, row[FIELD_SITE])
            files = list(os.listdir(sitedir))
            act = len(files)
            assert (
                act > 0
            ), f"Expected at least 1 attachment file in {sitedir}, but there were {act}"
            i += 1
        assert (
            i > 0
        ), f"Expected at least 1 site in attachments report, but there were {i}"


def assert_not_in_report_warnings_in_log(
    caplog: LogCaptureFixture, exp_files: list[str]
) -> None:
    act_warns = [r.message for r in caplog.records if r.levelname == "WARNING"]

    for exp_file in exp_files:
        assert any(
            re.match("attachment not in report", msg) is not None
            and re.search(re.escape(exp_file), msg) is not None
            for msg in act_warns
        ), f"Expected 'attachment not in report' log warning for file {exp_file}, but there was none"


def assert_not_in_source_warnings_in_log(
    caplog: LogCaptureFixture, exp_files: list[str]
) -> None:
    act_warns = [r.message for r in caplog.records if r.levelname == "WARNING"]

    for exp_file in exp_files:
        assert any(
            re.match("no attachments match report spec", msg) is not None
            and re.search(re.escape(exp_file), msg) is not None
            for msg in act_warns
        ), f"Expected 'no attachments match report spec' log warning for spec file {exp_file}, but there was none"


# HELPERS


def fixture_root_path() -> str:
    return os.path.join("test", "fixture", "test_move_attachments")


def output_root_path() -> str:
    return os.path.join("test", "output", "test_move_attachments")


def source_root_path(name: str) -> str:
    return os.path.join(fixture_root_path(), name, "rimageshare03")


def report_source_root_path(name: str) -> str:
    return os.path.join(fixture_root_path(), name, "ArchiveDump")


def destination_root_path(name: str) -> str:
    return os.path.join(output_root_path(), name)


def source_location(name: str, client: str, study: str) -> str:
    return os.path.join(source_root_path(name), f"{client}_{study}")


def report_source_location(name: str, client: str, study: str) -> str:
    return os.path.join(report_source_root_path(name), f"{client}_{study}")


def destination_location(name: str, client: str, study: str) -> str:
    return os.path.join(destination_root_path(name), f"{client}\\{study}")


def reset_path(path: str) -> None:
    rm_fr(path)
    mkdir_p(path)


def rm_fr(path: str) -> None:
    if not os.path.exists(path):
        return
    if os.path.relpath(path, ".").startswith("."):
        raise ValueError(f"Sorry Dave, I can't remove {path}.")
    shutil.rmtree(path)


def mkdir_p(path: str) -> None:
    os.makedirs(path, exist_ok=True)
