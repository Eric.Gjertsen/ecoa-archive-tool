import logging
import os.path
import os
import re
import shutil

import pytest
from _pytest.fixtures import FixtureRequest

from archive_tool.__main__ import process_sites
from archive_tool.model.config import (
    ProcessSitesConfig,
    ProcessSitesSourceDirConventions,
    ProcessSitesDestDirConventions,
    DistrConfig,
)
from archive_tool.model.exceptions import DestinationFileExistsError

logging.basicConfig(level=logging.DEBUG)

# FIXTURES


@pytest.fixture
def source_conventions() -> ProcessSitesSourceDirConventions:
    return ProcessSitesSourceDirConventions(
        data_dir="DataDir",
        dcr_dir="DCR",
        site_excludes=["DCF", "Device Data", "DOCS"],
        crf_dir_pattern=r"CRF\{site}",
        crf_toc_pattern=r"CRF\crftoc_{site}.pdf",
        dcr_match_pattern=r"_(?P<site>.+)_.+\.pdf",
        data_dir_pattern=r"DataDir\{site}",
        docs_dir_pattern=r"site histories\{site}",
        pdfs_dir_pattern=r"Sites\All Sites",
    )


@pytest.fixture
def destination_conventions() -> ProcessSitesDestDirConventions:
    return ProcessSitesDestDirConventions(
        crf_dir_pattern=r"Sites\{site}\CRF\{site}",
        crf_toc_pattern=r"Sites\{site}\CRF\crftoc.pdf",
        dcr_dir_pattern=r"Sites\{site}\DCR",
        data_dir_pattern=r"Sites\{site}\XML Data Directory",
        docs_dir_pattern=r"Sites\{site}\Documents",
        pdfs_dir_pattern=r"Sites\{site}\pdf",
    )


@pytest.fixture
def client() -> str:
    return "CLIENT"


@pytest.fixture
def study() -> str:
    return "STUDY"


@pytest.fixture
def dcr_prefix() -> str:
    return "CLIENT_STUDY"


# ------------------------------------------------------------------------------
# Custom distribution (SleepIZ)
# ------------------------------------------------------------------------------


@pytest.fixture
def label_sleepiz() -> str:
    return "SleepIZ"


@pytest.fixture
def source_sleepiz() -> str:
    return "sleepiz"


@pytest.fixture
def prefix_sleepiz() -> str:
    return "STUDY"


@pytest.fixture
def pattern_sleepiz() -> str:
    return r"_([^_]+)_SleepIZ\.csv"


@pytest.fixture
def custom_distr_sleepiz_nozip(
    label_sleepiz: str,
    source_sleepiz: str,
    prefix_sleepiz: str,
    pattern_sleepiz: str,
) -> DistrConfig:
    return DistrConfig(
        label=label_sleepiz,
        source_location=source_sleepiz,
        prefix=prefix_sleepiz,
        pattern=pattern_sleepiz,
        destination_pattern=r"Sites\{site}\SleepIZ",
        is_zipped=False,
        zip_prefix="",
        zip_pattern="",
    )


# ------------------------------------------------------------------------------
# Tests
# ------------------------------------------------------------------------------


def test_all_copy_no_overwrite_fails(
    request: FixtureRequest,
    source_conventions: ProcessSitesSourceDirConventions,
    destination_conventions: ProcessSitesDestDirConventions,
    client: str,
    study: str,
    dcr_prefix: str,
) -> None:
    name = request.node.name

    reset_path(destination_root_path(name))

    config = ProcessSitesConfig(
        dcr_prefix=dcr_prefix,
        source_conventions=source_conventions,
        destination_conventions=destination_conventions,
        source_location=source_location(name, client, study),
        custom_distr=None,
    )
    dst = destination_location(name, client, study)
    copy_overwrites_to_destination(config, dst)

    with pytest.raises(DestinationFileExistsError):
        process_sites(
            config,
            dst,
            client=client,
            study=study,
            overwrite=False,
            skip_if_exists=False,
            fail_fast=True,
        )


def test_all_copy(
    request: FixtureRequest,
    source_conventions: ProcessSitesSourceDirConventions,
    destination_conventions: ProcessSitesDestDirConventions,
    client: str,
    study: str,
    dcr_prefix: str,
) -> None:
    name = request.node.name

    reset_path(destination_root_path(name))

    config = ProcessSitesConfig(
        dcr_prefix=dcr_prefix,
        source_conventions=source_conventions,
        destination_conventions=destination_conventions,
        source_location=source_location(name, client, study),
        custom_distr=None,
    )
    dst = destination_location(name, client, study)

    copy_overwrites_to_destination(config, dst)
    process_sites(
        config,
        dst,
        client=client,
        study=study,
        overwrite=True,
        skip_if_exists=False,
        fail_fast=True,
    )

    # Note: this assumes all source sites have at least one file copied for
    # each of CRF, CRFTOC, DATA, DOCS, and PDFS. But not all source sites
    # necessarily have corresponding DCR files.

    sites = source_site_dirs(config)
    assert len(sites) > 0, "Expected to find one or more sites in source, found none"

    for site in sites:
        assert_files_exist_in(config.destination_crf_path(dst, site), "CRF")
        assert_file_exists(config.destination_crf_toc_path(dst, site), "CRFTOC")
        assert_files_exist_in(config.destination_site_data_path(dst, site), "DATA")
        assert_files_exist_in(config.destination_docs_path(dst, site), "DOCS")
        assert_files_exist_in(config.destination_pdfs_path(dst, site), "PDFS")

    dcr_site_files = source_dcr_site_files(config, dcr_prefix, sites)
    assert (
        len(dcr_site_files) > 0
    ), "Expected to find one or more DCR site-files in source, found none"

    for site, fname in dcr_site_files:
        assert_file_exists(
            os.path.join(config.destination_dcr_path(dst, site), fname), "DCR"
        )


def test_all_copy_with_sleepiz_nozip(
    request: FixtureRequest,
    source_conventions: ProcessSitesSourceDirConventions,
    destination_conventions: ProcessSitesDestDirConventions,
    client: str,
    study: str,
    dcr_prefix: str,
    custom_distr_sleepiz_nozip: DistrConfig,
) -> None:
    name = request.node.name

    reset_path(destination_root_path(name))

    config = ProcessSitesConfig(
        dcr_prefix=dcr_prefix,
        source_conventions=source_conventions,
        destination_conventions=destination_conventions,
        source_location=source_location(name, client, study),
        custom_distr=custom_distr_sleepiz_nozip,
    )
    dst = destination_location(name, client, study)

    copy_overwrites_to_destination(config, dst)
    process_sites(
        config,
        dst,
        client=client,
        study=study,
        overwrite=True,
        skip_if_exists=False,
        fail_fast=True,
    )

    sites = source_site_dirs(config)
    assert len(sites) > 0, "Expected to find one or more sites in source, found none"

    for site in sites:
        assert_files_exist_in(config.destination_crf_path(dst, site), "CRF")
        assert_file_exists(config.destination_crf_toc_path(dst, site), "CRFTOC")
        assert_files_exist_in(config.destination_site_data_path(dst, site), "DATA")
        assert_files_exist_in(config.destination_docs_path(dst, site), "DOCS")
        assert_files_exist_in(config.destination_pdfs_path(dst, site), "PDFS")

    sleepiz_files = source_custom_distr_files(config, sites)
    assert (
        len(sleepiz_files) > 0
    ), "Expected to find one or more SleepIZ files in source, found none"

    if config.custom_distr is None:  # Never
        return

    for site, fname in sleepiz_files:
        assert_file_exists(
            os.path.join(config.custom_distr.destination_path(dst, site), fname),
            config.custom_distr.label,
        )


# ASSERTIONS


def assert_file_exists(path: str, label: str) -> None:
    assert os.path.exists(path), f"Expected {label} destination file to exist: {path}"


def assert_files_exist_in(
    path: str, label: str, *, only_prefixed: str | None = None
) -> None:
    assert os.path.exists(path), f"Expected {label} destination path to exist: {path}"
    assert any(
        fname
        for fname in os.listdir(path)
        if only_prefixed is None or fname.lower().startswith(only_prefixed.lower())
    ), f"Expected {label} destination files in {path}, found none"


# HELPERS


def fixture_root_path() -> str:
    return os.path.join("test", "fixture", "test_process")


def output_root_path() -> str:
    return os.path.join("test", "output", "test_process")


def source_root_path(name: str) -> str:
    return os.path.join(fixture_root_path(), name)


def destination_root_path(name: str) -> str:
    return os.path.join(output_root_path(), name)


def source_location(name: str, client: str, study: str) -> str:
    return os.path.join(source_root_path(name), f"{client}_{study}")


def destination_location(name: str, client: str, study: str) -> str:
    return os.path.join(destination_root_path(name), f"{client}\\{study}")


def source_site_dirs(config: ProcessSitesConfig) -> list[str]:
    data_dir = config.source_data_path
    excls = config.source_site_excludes
    ret: list[str] = []
    for fname in os.listdir(data_dir):
        if os.path.isdir(os.path.join(data_dir, fname)) and fname not in excls:
            ret.append(fname)
    return ret


# TODO: refactor using source_custom_distr_files
def source_dcr_site_files(
    config: ProcessSitesConfig, exp_prefix: str, sites: list[str]
) -> list[tuple[str, str]]:
    dcr_dir = config.source_dcr_path
    ret: list[tuple[str, str]] = []
    for fname in os.listdir(dcr_dir):
        if not fname.lower().startswith(exp_prefix.lower()):
            continue
        m = re.match(
            config.source_dcr_match_pattern, fname[len(exp_prefix) : len(fname)]
        )
        if m is None:
            continue
        site: str = m.group(1)
        if os.path.isfile(os.path.join(dcr_dir, fname)) and site in sites:
            ret.append((site, fname))
    return ret


def source_custom_distr_files(
    config: ProcessSitesConfig, sites: list[str]
) -> list[tuple[str, str]]:
    ret: list[tuple[str, str]] = []
    src_dir = config.source_custom_distr_path
    if config.custom_distr is None or src_dir is None:
        return ret
    exp_prefix = config.custom_distr.prefix
    for fname in os.listdir(src_dir):
        if not fname.lower().startswith(exp_prefix.lower()):
            continue
        m = re.match(
            config.custom_distr.distr_file_pattern, fname[len(exp_prefix) : len(fname)]
        )
        if m is None:
            continue
        site: str = m.group(1)
        if os.path.isfile(os.path.join(src_dir, fname)) and site in sites:
            ret.append((site, fname))
    return ret


def reset_path(path: str) -> None:
    rm_fr(path)
    mkdir_p(path)


def copy_overwrites_to_destination(config: ProcessSitesConfig, dst: str) -> None:
    overwrite_path = os.path.join(config.source_location, ".overwrite")
    shutil.copytree(overwrite_path, dst)


def rm_fr(path: str) -> None:
    if not os.path.exists(path):
        return
    if os.path.relpath(path, ".").startswith("."):
        raise ValueError(f"Sorry Dave, I can't remove {path}.")
    shutil.rmtree(path)


def mkdir_p(path: str) -> None:
    os.makedirs(path, exist_ok=True)
