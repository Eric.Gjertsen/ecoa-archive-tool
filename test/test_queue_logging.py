import logging
import os.path
from queue import Queue
import sqlite3
from time import sleep

import pytest

# from _pytest.fixtures import FixtureRequest

from archive_tool.adapter.logging import queue_listener, bind_queue_handler
from archive_tool.adapter import report_db
from archive_tool.model.app_event import (
    AppEvent,
    Run,
    ProcessSiteCRF,
    encode as encode_app_event,
)
from archive_tool.model.config import (
    Config,
    ProcessSitesConfig,
    MoveAttachmentsConfig,
    ReportConventions,
    ProcessSitesSourceDirConventions,
    ProcessSitesDestDirConventions,
    MoveAttachmentsSourceDirConventions,
    MoveAttachmentsDestDirConventions,
)


# FIXTURES


@pytest.fixture
def queue() -> Queue[logging.LogRecord]:
    q: Queue[logging.LogRecord] = Queue()
    return q


@pytest.fixture
def timestamp() -> float:
    return 1719192310.0715637


@pytest.fixture
def connection() -> sqlite3.Connection:
    return report_db.init(":memory:")


@pytest.fixture
def username() -> str:
    return "joe.schmo"


@pytest.fixture
def config(
    client: str,
    study: str,
    destination_location: str,
    report_conventions: ReportConventions,
    process_sites_config: ProcessSitesConfig,
    move_attachments_config: MoveAttachmentsConfig,
) -> Config:
    return Config(
        client=client,
        study=study,
        overwrite=True,
        skip_if_exists=False,
        fail_fast=True,
        destination_location=destination_location,
        report=report_conventions,
        process_sites=process_sites_config,
        move_attachments=move_attachments_config,
    )


@pytest.fixture
def report_conventions() -> ReportConventions:
    return ReportConventions(
        log_pattern="report.log",
        db_pattern="report.db",
        report_pattern="report.xlsx",
    )


@pytest.fixture
def process_sites_config(
    dcr_prefix: str,
    source_location: str,
    process_sites_source_conventions: ProcessSitesSourceDirConventions,
    process_sites_destination_conventions: ProcessSitesDestDirConventions,
) -> ProcessSitesConfig:
    return ProcessSitesConfig(
        dcr_prefix=dcr_prefix,
        source_location=source_location,
        source_conventions=process_sites_source_conventions,
        destination_conventions=process_sites_destination_conventions,
        custom_distr=None,
    )


@pytest.fixture
def move_attachments_config(
    attachments_report: str,
    source_location: str,
) -> MoveAttachmentsConfig:
    return MoveAttachmentsConfig(
        copy_from_source=True,
        copy_report_from_source=True,
        attachments_report=attachments_report,
        source_location=source_location,
        report_source_location=source_location,
        source_conventions=MoveAttachmentsSourceDirConventions(
            attachments_dir="Attachments"
        ),
        destination_conventions=MoveAttachmentsDestDirConventions(
            work_dir="Work",
            dir_pattern=r"Sites\{site}\DCR\Attachments",
        ),
    )


@pytest.fixture
def source_location(client: str, study: str) -> str:
    return os.path.join(".", f"{client}_{study}")


@pytest.fixture
def destination_location(client: str, study: str) -> str:
    return os.path.join(".", f"{client}\\{study}")


@pytest.fixture
def process_sites_source_conventions() -> ProcessSitesSourceDirConventions:
    return ProcessSitesSourceDirConventions(
        data_dir="DataDir",
        dcr_dir="DCR",
        site_excludes=["dcf", "Device Data", "DOCS"],
        crf_dir_pattern=r"CRF\{site}",
        crf_toc_pattern=r"CRF\crftoc_{site}.pdf",
        dcr_match_pattern=r"(?P<prefix>.+)_(?P<site>.+)_[^_]+\.pdf",
        data_dir_pattern=r"DataDir\{site}",
        docs_dir_pattern=r"site histories\{site}",
        pdfs_dir_pattern=r"Sites\All Sites",
    )


@pytest.fixture
def process_sites_destination_conventions() -> ProcessSitesDestDirConventions:
    return ProcessSitesDestDirConventions(
        crf_dir_pattern=r"Sites\{site}\CRF\{site}",
        crf_toc_pattern=r"Sites\{site}\CRF\crftoc.pdf",
        dcr_dir_pattern=r"Sites\{site}\DCR",
        data_dir_pattern=r"Sites\{site}\XML Data Directory",
        docs_dir_pattern=r"Sites\{site}\Documents",
        pdfs_dir_pattern=r"Sites\{site}\pdf",
    )


@pytest.fixture
def dcr_prefix() -> str:
    return "DCR"


@pytest.fixture
def attachments_report() -> str:
    return "attachments.rpt"


@pytest.fixture
def client() -> str:
    return "CLIENT"


@pytest.fixture
def study() -> str:
    return "STUDY"


@pytest.fixture
def site() -> str:
    return "001"


@pytest.fixture
def src_file() -> str:
    return r"path\to\src\file"


@pytest.fixture
def dst_file() -> str:
    return r"path\to\dst\file"


@pytest.fixture
def app_event_run_start(
    timestamp: float,
    username: str,
    client: str,
    study: str,
) -> AppEvent:
    return Run(
        timestamp=timestamp,
        username=username,
        client=client,
        study=study,
    )


@pytest.fixture
def app_event_run_complete(
    timestamp: float,
    username: str,
    client: str,
    study: str,
) -> Run:
    return Run(
        timestamp=timestamp,
        username=username,
        client=client,
        study=study,
        complete=True,
    )


@pytest.fixture
def app_event_process_site_crf_not_complete(
    client: str,
    study: str,
    site: str,
    src_file: str,
    dst_file: str,
) -> ProcessSiteCRF:
    return ProcessSiteCRF(
        client=client,
        study=study,
        site=site,
        src_file=src_file,
        dst_file=dst_file,
        complete=False,
        error=None,
    )


# TESTS


def test_simple(
    timestamp: float,
    queue: Queue[logging.LogRecord],
    connection: sqlite3.Connection,
    app_event_run_start: Run,
    app_event_process_site_crf_not_complete: ProcessSiteCRF,
    app_event_run_complete: Run,
) -> None:
    logging.basicConfig(level=logging.DEBUG)
    logger = logging.getLogger(__name__)

    listener = queue_listener(connection, timestamp, queue)
    bind_queue_handler(logger, queue)

    listener.start()

    logger.info("run start", extra=encode_app_event(app_event_run_start))
    sleep(0.1)

    logger.info(
        "testing", extra=encode_app_event(app_event_process_site_crf_not_complete)
    )
    sleep(0.1)

    logger.info("run complete", extra=encode_app_event(app_event_run_complete))
    sleep(0.1)

    listener.stop()

    assert_db_has_no_runtime_error_records(connection, timestamp)
    assert_db_has_run_complete_record(connection, timestamp)
    assert_db_has_process_site_record(
        connection, timestamp, app_event_process_site_crf_not_complete
    )


"""
def test_sublogger(
    timestamp: float,
    queue: Queue[logging.LogRecord],
    connection: sqlite3.Connection,
    app_event_process_site_crf_not_complete: AppEvent,
) -> None:
    logging.basicConfig(level=logging.DEBUG)
    logger = logging.getLogger(__name__)
    sub_logger = logging.getLogger(__name__ + ".sublogger")

    listener = queue_listener(connection, timestamp, queue)
    bind_queue_handler(logger, queue)

    listener.start()

    sub_logger.info(
        "testing", extra=encode_app_event(app_event_process_site_crf_not_complete)
    )

    sleep(0.1)
    listener.stop()
"""


def assert_db_has_no_runtime_error_records(
    db: sqlite3.Connection, tstamp: float
) -> None:
    c = db.execute("SELECT * FROM `runtime_error` WHERE timestamp = ?", (int(tstamp),))
    rs = c.fetchall()
    act_n = len(rs)
    assert act_n == 0, f"Expected 0 `runtime_error` records, but there are {act_n}"


def assert_db_has_run_complete_record(db: sqlite3.Connection, tstamp: float) -> None:
    c = db.execute("SELECT `complete` FROM `run` WHERE timestamp = ?", (int(tstamp),))
    rs = c.fetchall()
    act_n = len(rs)
    assert act_n == 1, f"Expected 1 `run` record, but there are {act_n}"
    act_complete = int(rs[0][0])
    assert (
        act_complete == 1
    ), f"Expected `run` record complete = 1, but is {act_complete}"


def assert_db_has_process_site_record(
    db: sqlite3.Connection, tstamp: float, event: ProcessSiteCRF
) -> None:
    c = db.execute(
        "SELECT * FROM `process_site` WHERE run_timestamp = ?", (int(tstamp),)
    )
    c.row_factory = sqlite3.Row  # type: ignore
    rs = c.fetchall()
    act_n = len(rs)
    assert act_n == 1, f"Expected 1 `process_site` record, but there are {act_n}"
    act_row: sqlite3.Row = rs[0]
    print(dict(act_row))
    assert act_row["stage"] == "ProcessSiteCRF"
    assert act_row["client"] == event.client
    assert act_row["study"] == event.study
    assert act_row["site"] == event.site
    assert act_row["src_file"] == event.src_file
    assert act_row["dst_file"] == event.dst_file
    assert act_row["complete"] == (1 if event.complete else 0)
