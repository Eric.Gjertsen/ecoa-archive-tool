import csv
from itertools import chain
import os.path
import os
import random
import shutil
import string
import tomli_w
from typing import Callable

from archive_tool.model.config import (
    ProcessSitesSourceDirConventions,
    ProcessSitesDestDirConventions,
    MoveAttachmentsSourceDirConventions,
    MoveAttachmentsDestDirConventions,
    Config,
    ProcessSitesConfig,
    MoveAttachmentsConfig,
    ReportConventions,
)


class TestFilesGenerator(object):
    __test__ = False

    def __init__(
        self,
        *,
        client: str,
        study: str,
        source: str,
        attachments_source: str,
        expected: str,
        src_conventions: ProcessSitesSourceDirConventions,
        dst_conventions: ProcessSitesDestDirConventions,
        attachments_src_conventions: MoveAttachmentsSourceDirConventions,
        attachments_dst_conventions: MoveAttachmentsDestDirConventions,
        dcr_prefix: str,
        sites: set[str],
        pdfs: set[str],
        attachments_report: str,
    ) -> None:
        self.client = client
        self.study = study
        self.source = source
        self.attachments_source = attachments_source
        self.expected = expected
        self.src_conventions = src_conventions
        self.dst_conventions = dst_conventions
        self.attachments_src_conventions = attachments_src_conventions
        self.attachments_dst_conventions = attachments_dst_conventions
        self.dcr_prefix = dcr_prefix
        self.sites = sites
        self.pdfs = pdfs
        self.attachments_report = attachments_report

    def generate(
        self,
        *,
        min_files: int = 1,
        max_files: int = 10,
        missing_data: set[str] = set(),
        missing_crf: set[str] = set(),
        missing_crf_toc: set[str] = set(),
        missing_dcrs: set[str] = set(),
        missing_history: set[str] = set(),
        extra_attachments: int = 0,
    ) -> None:
        site_subjs = {
            site: random_subject_ids(min_files, max_files) for site in self.sites
        }
        site_dcrs = {site: random_dcr_ids(0, max_files) for site in self.sites}
        attachment_recs_repo, attachments_repo = random_attachments_repo(
            self.sites, min_files, max_files
        )
        attachment_recs_file, attachments_file = random_attachments_file_name(
            self.sites, min_files, max_files
        )
        attachment_recs_dcf, attachments_dcf = random_attachments_dcf(
            self.sites, min_files, max_files
        )
        attachment_recs_repo_suffix, attachments_repo_suffix = (
            random_attachments_repo_suffix(self.sites, min_files, max_files)
        )

        self.clear()
        self.create_data_files(site_subjs, missing=missing_data)
        self.create_crf_files(site_subjs, missing=missing_crf)
        self.create_crf_toc_files(missing=missing_crf_toc)
        self.create_dcr_files(site_dcrs, missing=missing_dcrs)
        self.create_history_files(missing=missing_history)
        self.create_pdf_files()
        self.create_attachments_report(
            attachment_recs_repo
            + attachment_recs_file
            + attachment_recs_dcf
            + attachment_recs_repo_suffix
        )
        self.create_attachments(
            attachments_repo
            + attachments_file
            + attachments_dcf
            + attachments_repo_suffix
        )

    def create_destination(
        self,
        destination: str,
    ) -> None:
        """
        Note: this is to copy over the "All Sites" PDF files to the destination,
        which is where process_sites looks for them. In real life these are
        created manually in the destination - not copied from the source.
        """
        src = self.src_conventions.pdfs_path(self.source, "")
        dst = self.src_conventions.pdfs_path(destination, "")
        shutil.copytree(src, dst)

    def generate_config_file(
        self,
        config_file: str,
        *,
        destination: str,
        overwrite: bool = True,
        skip_if_exists: bool = True,
        fail_fast: bool = True,
    ) -> None:
        # print(destination)
        config = Config(
            client=self.client,
            study=self.study,
            overwrite=overwrite,
            skip_if_exists=skip_if_exists,
            fail_fast=fail_fast,
            destination_location=destination,
            report=ReportConventions(
                log_pattern="archive_tool.log",
                db_pattern="archive_tool.db",
                report_pattern="archive_tool.xlsx",
            ),
            process_sites=ProcessSitesConfig(
                dcr_prefix=self.dcr_prefix,
                source_location=self.source,
                source_conventions=self.src_conventions,
                destination_conventions=self.dst_conventions,
                custom_distr=None,
            ),
            move_attachments=MoveAttachmentsConfig(
                copy_from_source=True,
                copy_report_from_source=True,
                attachments_report=self.attachments_report,
                source_location=self.attachments_source,
                report_source_location=self.attachments_source,
                source_conventions=self.attachments_src_conventions,
                destination_conventions=self.attachments_dst_conventions,
            ),
        )
        config_path = os.path.join(destination, config_file)
        exp_config_path = os.path.join(self.expected, config_file)

        mkdir_p(os.path.dirname(config_path))
        mkdir_p(os.path.dirname(exp_config_path))

        with open(config_path, "wb") as f, open(exp_config_path, "wb") as g:
            data = {"archive_tool": config.to_dict()}
            tomli_w.dump(data, f)
            tomli_w.dump(data, g)

    def clear(self) -> None:
        rm_rf(self.source)
        rm_rf(self.attachments_source)
        rm_rf(self.expected)

    def create_data_files(
        self, site_subjs: dict[str, set[str]], missing: set[str] = set()
    ) -> None:
        for src, dst in self.data_file_pairs(site_subjs, missing):
            if dst is None:
                write_random_text_file(src, min_size=0, max_size=4096)
            else:
                write_random_text_file_pairs(src, dst, min_size=0, max_size=4096)

        for excl in self.source_data_dir_excludes:
            mkdir_p(excl)

    def create_crf_files(
        self, site_subjs: dict[str, set[str]], missing: set[str] = set()
    ) -> None:
        for src, dst in self.crf_file_pairs(site_subjs, missing):
            if dst is None:
                write_random_binary_file(src, min_size=0, max_size=4096)
            else:
                write_random_binary_file_pairs(src, dst, min_size=0, max_size=4096)

    def create_crf_toc_files(self, missing: set[str] = set()) -> None:
        for src, dst in self.crf_toc_file_pairs(missing):
            if dst is None:
                write_random_binary_file(src, min_size=0, max_size=4096)
            else:
                write_random_binary_file_pairs(src, dst, min_size=0, max_size=4096)

    def create_dcr_files(
        self, site_dcrs: dict[str, set[str]], missing: set[str] = set()
    ) -> None:
        for src, dst in self.dcr_file_pairs(self.dcr_prefix, site_dcrs, missing):
            if dst is None:
                write_random_binary_file(src, min_size=0, max_size=4096)
            else:
                write_random_binary_file_pairs(src, dst, min_size=0, max_size=4096)

    def create_history_files(self, missing: set[str] = set()) -> None:
        for src, dst in self.history_file_pairs(missing):
            if dst is None:
                write_random_binary_file(src, min_size=0, max_size=4096)
            else:
                write_random_binary_file_pairs(src, dst, min_size=0, max_size=4096)

    def create_pdf_files(self) -> None:
        for pdf in self.pdfs:
            src = os.path.join(self.src_conventions.pdfs_path(self.source, ""), pdf)
            for site in self.sites:
                dst = os.path.join(
                    self.dst_conventions.pdfs_path(self.expected, site), pdf
                )
                write_random_binary_file_pairs(src, dst, min_size=0, max_size=4096)

    def create_attachments_report(self, recs: list[dict[str, str]]) -> None:
        rpt_file = os.path.join(self.attachments_source, self.attachments_report)
        exp_rpt_file = os.path.join(self.expected, self.attachments_report)
        hdrs = ["dcf_id", "repository_id", "file_name", "SiteCode"]
        mkdir_p(os.path.dirname(rpt_file))
        mkdir_p(os.path.dirname(exp_rpt_file))
        with open(rpt_file, "w+", newline="") as src_f, open(
            exp_rpt_file, "w+", newline=""
        ) as dst_f:
            src_csv = csv.DictWriter(src_f, fieldnames=hdrs, delimiter="|")
            dst_csv = csv.DictWriter(dst_f, fieldnames=hdrs, delimiter="|")
            src_csv.writeheader()
            dst_csv.writeheader()
            for rec in recs:
                src_csv.writerow(rec)
                dst_csv.writerow(rec)

    def create_attachments(self, attachment_sites: list[tuple[str, str]]) -> None:
        for attachment_file, site in attachment_sites:
            src = os.path.join(
                self.attachments_src_conventions.attachments_path(
                    self.attachments_source
                ),
                attachment_file,
            )
            dst = os.path.join(
                self.attachments_dst_conventions.attachments_path(self.expected, site),
                attachment_file,
            )
            write_random_binary_file_pairs(src, dst, min_size=0, max_size=4096)

    def data_file_pairs(
        self, site_subjs: dict[str, set[str]], missing: set[str] = set()
    ) -> list[tuple[str, str | None]]:
        return file_pairs(
            src_path=lambda site: self.src_conventions.site_data_path(
                self.source, site
            ),
            dst_path=lambda site: self.dst_conventions.site_data_path(
                self.expected, site
            ),
            sites=self.sites,
            site_subjs=site_subjs,
            file_pattern="p{subj}.xml",
            missing=missing,
        )

    def crf_file_pairs(
        self, site_subjs: dict[str, set[str]], missing: set[str] = set()
    ) -> list[tuple[str, str | None]]:
        return file_pairs(
            src_path=lambda site: self.src_conventions.crf_path(self.source, site),
            dst_path=lambda site: self.dst_conventions.crf_path(self.expected, site),
            sites=self.sites,
            site_subjs=site_subjs,
            file_pattern="{subj}.pdf",
            missing=missing,
        )

    def crf_toc_file_pairs(
        self, missing: set[str] = set()
    ) -> list[tuple[str, str | None]]:
        return file_pairs(
            src_path=lambda site: self.src_conventions.crf_toc_path(self.source, site),
            dst_path=lambda site: self.dst_conventions.crf_toc_path(
                self.expected, site
            ),
            sites=self.sites,
            site_subjs={site: {""} for site in self.sites},
            file_pattern=None,
            missing=missing,
        )

    def dcr_file_pairs(
        self,
        dcr_prefix: str,
        site_dcrs: dict[str, set[str]],
        missing: set[str] = set(),
    ) -> list[tuple[str, str | None]]:
        return file_pairs(
            src_path=lambda site: self.src_conventions.dcr_path(self.source),
            dst_path=lambda site: self.dst_conventions.dcr_path(self.expected, site),
            sites=self.sites,
            site_subjs=site_dcrs,
            file_pattern=dcr_prefix + "_{site}_{subj}.pdf",
            missing=missing,
        )

    def history_file_pairs(
        self, missing: set[str] = set()
    ) -> list[tuple[str, str | None]]:
        return file_pairs(
            src_path=lambda site: self.src_conventions.docs_path(self.source, site),
            dst_path=lambda site: self.dst_conventions.docs_path(self.expected, site),
            sites=self.sites,
            site_subjs={site: {""} for site in self.sites},
            file_pattern="History.pdf",
            missing=missing,
        )

    @property
    def source_data_dir_excludes(self) -> list[str]:
        return [
            os.path.join(self.src_conventions.data_path(self.source), excl)
            for excl in self.src_conventions.site_excludes
        ]


def random_subject_ids(min_size: int, max_size: int) -> set[str]:
    return set(
        "%03d" % random.randrange(0, 999)
        for _ in range(random.randrange(min_size, max_size))
    )


def random_dcr_ids(min_size: int, max_size: int) -> set[str]:
    return set(
        "SW" + random_dcf_id() for _ in range(random.randrange(min_size, max_size))
    )


def random_dcf_id() -> str:
    return "%06d" % random.randrange(0, 999999)


def random_repository_id() -> str:
    return "%06d" % random.randrange(0, 999999)


def random_file_name(exts: list[str], min_size: int = 1, max_size: int = 10) -> str:
    return "{0}.{1}".format(
        random_alphanumeric_text(min_size, max_size), random.choice(exts)
    )


def random_alphanumeric_text(min_size: int, max_size: int) -> str:
    return "".join(
        random.choices(
            string.ascii_letters + string.digits + " -_.",
            k=random.randrange(min_size, max_size),
        )
    )


def file_pairs(
    *,
    src_path: Callable[[str], str],
    dst_path: Callable[[str], str],
    sites: set[str],
    site_subjs: dict[str, set[str]],
    file_pattern: str | None,
    missing: set[str] = set(),
) -> list[tuple[str, str | None]]:
    empty_set: set[str] = set()
    return list(
        chain.from_iterable(
            [
                file_pairs_for_site(
                    src_path=src_path(site),
                    dst_path=dst_path(site),
                    site=site,
                    subjs=site_subjs.get(site, empty_set),
                    file_pattern=file_pattern,
                    missing=missing,
                )
                for site in sites
            ]
        )
    )


def file_pairs_for_site(
    *,
    src_path: str,
    dst_path: str,
    site: str,
    subjs: set[str],
    file_pattern: str | None,
    missing: set[str],
) -> list[tuple[str, str | None]]:
    return [
        (
            src_path
            if file_pattern is None
            else os.path.join(src_path, file_pattern.format(site=site, subj=subj)),
            None
            if site in missing
            else (
                dst_path
                if file_pattern is None
                else os.path.join(dst_path, file_pattern.format(site=site, subj=subj))
            ),
        )
        for subj in subjs
    ]


def random_attachments_repo(
    sites: set[str], min_files: int, max_files: int
) -> tuple[list[dict[str, str]], list[tuple[str, str]]]:
    return random_attachments(
        sites=sites,
        record_file_name="",
        pattern="{opaque}_{repository_id}.pdf",
        min_files=min_files,
        max_files=max_files,
    )


def random_attachments_file_name(
    sites: set[str], min_files: int, max_files: int
) -> tuple[list[dict[str, str]], list[tuple[str, str]]]:
    return random_attachments(
        sites=sites,
        pattern="{file_name}",
        min_files=min_files,
        max_files=max_files,
    )


def random_attachments_dcf(
    sites: set[str], min_files: int, max_files: int
) -> tuple[list[dict[str, str]], list[tuple[str, str]]]:
    return random_attachments(
        sites=sites,
        pattern="{base_name}_{dcf_id}{ext_name}",
        min_files=min_files,
        max_files=max_files,
        reverse_pattern=True,
    )


def random_attachments_repo_suffix(
    sites: set[str], min_files: int, max_files: int
) -> tuple[list[dict[str, str]], list[tuple[str, str]]]:
    return random_attachments(
        sites=sites,
        pattern="{base_name}_{repository_id}{ext_name}",
        min_files=min_files,
        max_files=max_files,
    )


def random_attachments(
    *,
    sites: set[str],
    pattern: str,
    min_files: int,
    max_files: int,
    record_file_name: str | None = None,
    reverse_pattern: bool = False,
) -> tuple[list[dict[str, str]], list[tuple[str, str]]]:
    recs: list[dict[str, str]] = []
    fname_sites: list[tuple[str, str]] = []
    for _ in range(random.randrange(min_files, max_files)):
        if reverse_pattern:
            rec, fs = random_attachments_pair_reverse_pattern(
                sites=list(sites),
                record_file_name=record_file_name,
                pattern=pattern,
            )
        else:
            rec, fs = random_attachments_pair(
                sites=list(sites),
                record_file_name=record_file_name,
                pattern=pattern,
                min_files=min_files,
                max_files=max_files,
            )
        recs = recs + [rec]
        fname_sites = fname_sites + fs
    return (recs, fname_sites)


def random_attachments_pair(
    *,
    sites: list[str],
    record_file_name: str | None,
    pattern: str,
    min_files: int,
    max_files: int,
) -> tuple[dict[str, str], list[tuple[str, str]]]:
    rec = random_attachments_record(sites, record_file_name)
    fname_sites: list[tuple[str, str]] = []
    for _ in range(random.randrange(min_files, max_files)):
        opaque = random_alphanumeric_text(1, 10)
        base_name, ext_name = os.path.splitext(rec["file_name"])
        fname_sites.append(
            (
                pattern.format(
                    opaque=opaque, base_name=base_name, ext_name=ext_name, **rec
                ),
                rec["SiteCode"],
            )
        )
    return (rec, fname_sites)


def random_attachments_pair_reverse_pattern(
    *,
    sites: list[str],
    record_file_name: str | None,
    pattern: str,
) -> tuple[dict[str, str], list[tuple[str, str]]]:
    rec = random_attachments_record(sites, record_file_name)
    file_name = rec["file_name"]
    base_name, ext_name = os.path.splitext(file_name)
    rec["file_name"] = pattern.format(base_name=base_name, ext_name=ext_name, **rec)
    fname_sites: list[tuple[str, str]] = []
    fname_sites.append((file_name, rec["SiteCode"]))
    return (rec, fname_sites)


def random_attachments_record(
    sites: list[str], file_name: str | None = None
) -> dict[str, str]:
    return {
        "dcf_id": random_dcf_id(),
        "repository_id": random_repository_id(),
        "file_name": random_file_name(["pdf", "docx", "rtf"])
        if file_name is None
        else file_name,
        "SiteCode": random.choice(sites),
    }


def write_random_text_file(fname: str, *, min_size: int, max_size: int) -> None:
    mkdir_p(os.path.dirname(fname))
    with open(fname, "w+") as f:
        size = random.randrange(min_size, max_size)
        f.write("".join(chr(random.randint(32, 126)) for _ in range(size)))


def write_random_binary_file(fname: str, *, min_size: int, max_size: int) -> None:
    mkdir_p(os.path.dirname(fname))
    with open(fname, "wb+") as f:
        size = random.randrange(min_size, max_size)
        f.write(random.randbytes(size))


def write_random_text_file_pairs(
    fname1: str, fname2: str, *, min_size: int, max_size: int
) -> None:
    mkdir_p(os.path.dirname(fname1))
    mkdir_p(os.path.dirname(fname2))
    with open(fname1, "w+") as f, open(fname2, "w+") as g:
        size = random.randrange(min_size, max_size)
        txt = "".join(chr(random.randint(32, 126)) for _ in range(size))
        f.write(txt)
        g.write(txt)


def write_random_binary_file_pairs(
    fname1: str, fname2: str, *, min_size: int, max_size: int
) -> None:
    mkdir_p(os.path.dirname(fname1))
    mkdir_p(os.path.dirname(fname2))
    with open(fname1, "wb+") as f, open(fname2, "wb+") as g:
        size = random.randrange(min_size, max_size)
        data = random.randbytes(size)
        f.write(data)
        g.write(data)


def mkdir_p(path: str) -> None:
    os.makedirs(path, exist_ok=True)


def rm_rf(path: str) -> None:
    if not os.path.exists(path):
        return
    if os.path.relpath(path, ".").startswith("."):
        raise ValueError(f"Sorry Dave, I can't remove {path}.")
    shutil.rmtree(path)
